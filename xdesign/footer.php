<?php $footerGroup = get_field('footer', 'options') ;?>

<footer class="footer">
  <div class="container">
    <div class="cols footer__upper-cols">
      <div class="col is-8 is-3-md footer__contact-container">
			<?php
				if($footerGroup['logo']) {
					?> 
						<a href="/">
							<img src="<?php echo $footerGroup['logo'];?>" class="header__logo" alt="header-logo">
						</a> 
					<?php
				}
				else {
					?> 
						<a href="/">
              <svg width="220" height="30" viewBox="0 0 220 30" fill="none" alt="logo" xmlns="http://www.w3.org/2000/svg">
              <path d="M21.9833 15.0022L36.7017 29.1789H28.8547L18.3317 18.9517L7.84436 29.1789H0L14.6829 15.0022L0.00273589 0.789905H7.84436L18.3317 11.0171L28.8547 0.789905H36.699L21.9833 15.0022ZM69.8968 7.14774C71.0447 9.26503 71.6269 11.899 71.6269 15.0052C71.6269 18.1114 71.0447 20.7038 69.8968 22.8211C68.8131 24.8761 67.1973 26.5398 65.2503 27.605C63.2428 28.6698 61.0336 29.2088 58.7999 29.1789H40.2467V0.789905H58.7971C61.031 0.7586 63.2405 1.29769 65.2475 2.36377C67.1962 3.42758 68.8132 5.09148 69.8968 7.14774ZM45.5437 23.9703H56.4438C58.7151 23.9703 60.5628 23.712 61.9321 23.2042C63.2086 22.7571 64.2869 21.8111 64.9578 20.5494C65.6493 19.2606 66.0019 17.3927 66.0019 15.0052C66.0019 12.6177 65.6493 10.7082 64.9578 9.41945C64.2869 8.15776 63.2086 7.21168 61.9321 6.76466C60.5655 6.2539 58.7124 5.99555 56.4438 5.99555H45.5437V23.9703ZM74.8904 29.1789H102.359V24.0891H80.1791V17.5293H101.635V12.4543H80.1791V5.92131H102.348V0.789905H74.8904V29.1789ZM136.738 15.5694C135.467 14.4469 133.731 13.6808 131.602 13.2918C129.511 12.9087 126.384 12.5583 122.309 12.2435C119.415 12.0356 117.233 11.7921 115.826 11.5219C114.459 11.2576 113.516 10.8924 113.008 10.435C112.765 10.2089 112.573 9.92414 112.45 9.60342C112.326 9.28271 112.273 8.93496 112.295 8.58797C112.281 8.23058 112.337 7.87412 112.46 7.54232C112.583 7.21052 112.77 6.911 113.008 6.6637C113.503 6.15293 114.402 5.75204 115.686 5.47587C117.012 5.17892 118.893 5.04825 121.273 5.04825C123.399 5.00722 125.524 5.17619 127.622 5.55308C129.172 5.8827 130.306 6.38753 130.995 7.06162C131.329 7.39269 131.591 7.79963 131.763 8.2534C131.935 8.70716 132.012 9.19657 131.99 9.68671V10.049H137.618V9.68374C137.593 6.48849 136.156 4.04158 133.351 2.40535C130.618 0.807723 126.753 0 121.891 0C117.028 0 113.3 0.632517 110.709 1.87973C108.031 3.17149 106.672 5.34818 106.672 8.35041C106.672 10.4558 107.219 12.1485 108.312 13.3808C109.406 14.6132 111.045 15.5071 113.232 16.098C115.419 16.6889 118.373 17.0928 122.093 17.3274C125.447 17.562 127.907 17.7996 129.402 18.0341C130.848 18.2598 131.843 18.5984 132.359 19.0408C132.876 19.4833 133.086 20.1069 133.086 21.0215C133.086 22.2895 132.37 23.2249 130.9 23.8782C129.355 24.5642 126.764 24.9117 123.2 24.9117C118.778 24.9117 115.637 24.4662 113.866 23.5872C112.155 22.7379 111.324 21.5085 111.324 19.8426V19.4803H105.642L105.661 19.8604C105.836 23.5278 107.454 26.1678 110.469 27.703C113.412 29.1878 117.706 29.9629 123.236 29.9629C128.016 29.9629 131.807 29.3036 134.507 28.003C137.295 26.6607 138.708 24.2197 138.708 20.7454C138.708 18.4677 138.044 16.7275 136.738 15.5694ZM142.324 29.1789H147.624V0.789905H142.324V29.1789ZM167.164 14.0549V18.9428H178.212V22.8478C176.771 23.53 175.27 24.0523 173.732 24.4068C171.928 24.7587 170.098 24.9208 168.266 24.8909C165.259 24.8909 162.87 24.3385 161.192 23.2487C159.514 22.1589 158.363 20.8731 157.754 19.415C157.162 18.0502 156.845 16.564 156.824 15.0557C156.824 14.7383 156.839 14.4212 156.868 14.1054C156.973 12.9227 157.243 11.7642 157.669 10.6696C157.75 10.4662 157.841 10.268 157.942 10.0757C158.543 8.8879 159.547 7.80104 160.93 6.87157C162.548 5.78471 164.874 5.23534 167.836 5.23534C170.799 5.23534 173.185 5.76689 174.899 6.82108C176.266 7.6585 177.179 8.72754 177.66 10.0876H183.446C183.096 8.3564 182.357 6.74635 181.295 5.40163C180.021 3.75947 178.22 2.43207 175.946 1.48181C173.672 0.531553 170.955 0.0386058 167.836 0.0386058C164.401 0.0386058 161.4 0.653304 158.918 1.85895C156.436 3.06459 154.479 4.82851 153.159 7.09428C152.612 8.02818 152.176 9.03406 151.863 10.0876C151.473 11.3947 151.246 12.7532 151.191 14.1262C151.175 14.4232 151.169 14.7201 151.169 15.0408C151.169 18.0638 151.866 20.7305 153.236 22.9725C154.605 25.2146 156.603 26.9636 159.175 28.1811C161.747 29.3987 164.778 29.9985 168.235 29.9985C170.811 30.0252 173.378 29.6909 175.875 29.0037C178.509 28.1942 181.044 27.0479 183.429 25.5887L183.602 25.4848V14.0549H167.164ZM214.722 0.789905V23.1448L194.865 0.899777L194.767 0.789905H187.66V29.1789H192.96V6.8389L212.828 29.0839L212.924 29.1938H220V0.789905H214.722Z" fill="#090821"/>
              </svg>
						</a>
					<?php
				}
			?>
        <strong class="footer__sub">
          <?php echo $footerGroup['logo_sub_text']; ?>
        </strong>
  
        <p class="footer__description">
          <?php echo $footerGroup['logo_description']; ?>
        </p>
  
        <?php if($footerGroup['button_text'] && $footerGroup['button_url']) : ?>
          <a class="footer__button primary-button primary-button--<?php echo $footerGroup['button_colour']; ?>" href="mailto:<?php echo $footerGroup['button_url'];?>?subject=<?php echo $footerGroup['button_subject']; ?>">
            <span class="primary-button__text">
              <?php echo $footerGroup['button_text']; ?>
            </span>
            <span class="primary-button__icon"></span>
          </a>
        <?php endif; ?>
      </div>
  
      <div class="col is-12 is-offset-1-md is-4-md no-pb">
        <ul class="footer__links">
          <?php while(have_rows('footer', 'options')) : the_row(); ?>
            <?php while (have_rows('column_1' )) : the_row();?>
              <li>
                <a class="footer__link" href="<?php echo get_sub_field('url');?>">
                  <?php echo get_sub_field('text'); ?>
                  <svg class="footer__link-arrow" width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M15.693 10.3328L0.79999 10.3328L0.799991 7.66766L15.693 7.66766L11.1329 1.85757L13.0608 0.151857L20 9.00558L13.0608 17.8486L11.1329 16.1429L15.693 10.3328Z" fill="currentColor"/>
                  </svg>
                </a>
              </li>
            <?php endwhile; ?>
          <?php endwhile; ?>
        </ul>
      </div>

      <div class="col is-12 is-4-md">
        <ul class="footer__links">
          <?php while(have_rows('footer', 'options')) : the_row(); ?>
            <?php while (have_rows('column_2' )) : the_row();?>
              <li>
                <a class="footer__link" href="<?php echo get_sub_field('url');?>">
                  <?php echo get_sub_field('text'); ?>
                  <svg class="footer__link-arrow" width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M15.693 10.3328L0.79999 10.3328L0.799991 7.66766L15.693 7.66766L11.1329 1.85757L13.0608 0.151857L20 9.00558L13.0608 17.8486L11.1329 16.1429L15.693 10.3328Z" fill="currentColor"/>
                  </svg>
                </a>
              </li>
            <?php endwhile; ?>
          <?php endwhile; ?>
        </ul>
      </div>
    </div>

    <div class="cols">
      <div class="col is-12 no-pb">
        <div class="footer__separator"></div>
      </div>
      <div class="col is-12 is-4-md footer__socials">
        <div>
          <?php while(have_rows('footer', 'options')) : the_row(); ?>
            <?php while (have_rows('social' )) : the_row();?>
              <a target="_blank" class="footer__social-link" href="<?php echo get_sub_field('url'); ?>">
                <img src="<?php echo get_sub_field('icon')['url'];?>" alt="<?php echo get_sub_field('icon')['alt'];?>">
                <span><?php echo get_sub_field('name'); ?></span>
              </a>
            <?php endwhile; ?>
          <?php endwhile; ?>
        </div>
  
          <p class="footer__copyright">
            <?php echo $footerGroup['copyright_text']; ?>
          </p>
      </div>
  
      <div class="col is-12 is-8-md footer__contacts">
        <?php while(have_rows('footer', 'options')) : the_row(); ?>
          <?php while(have_rows('contact_info')) : the_row(); ?>
            <div class="footer__contact-info">
              <strong class="footer__contact-city">
                <?php echo get_sub_field('location'); ?>
              </strong>
    
              <p class="footer__contact-location">
                <?php echo get_sub_field('office_location'); ?>
              </p>
    
              <span class="footer__contact-country">
                <?php echo get_sub_field('country');?>
              </span>
    
              <a class="footer__contact-phone" href="tel:<?php echo str_replace(" ", "", get_sub_field('phone'));?>">
                <strong>
                  <?php echo get_sub_field('phone');?>
                </strong>
              </a>
            </div>
          <?php endwhile; ?>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

</body>


</html>
