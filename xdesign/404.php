<?php get_header(); ?>

<div class="not-found">
  <div class="container">
    <div class="cols">

      <div class="col is-12 is-6-md no-pb load-hidden">
        <h1 class="not-found__title gradient-text gradient-blue-purple">
          404
        </h1>
      </div>

      <div class="col is-12 is-6-md no-pb">
        <h3 class="not-found__sub-title load-hidden">
          It’s not you, it’s us.
        </h3>
        <p class="not-found__description load-hidden">
          It looks like the page you’re searching for isn’t available at the moment. We’re probably working on improving it, or it may have been moved.
          Return to the careers <a style="textDecoration:none;color:white;" href="https://www.xdesign.com/careers">home page</a>, or if you’d prefer to contact us, just <a style="textDecoration:none;color:white;" href="mailto:mailto:talent@xdesign.com">email</a> us and a member of our team will be in touch.
        </p>
        <a href="/" class="primary-button primary-button--purple load-hidden">
          <span class="primary-button__text">
            Return to homepage
          </span>  
          <span class="primary-button__icon"></span>
        </a>
      </div>

    </div>
  </div>
</div>

<?php get_footer(); ?>
