document.addEventListener("DOMContentLoaded", () => {
  
    /*! modernizr 3.6.0 (Custom Build) | MIT *
    * https://modernizr.com/download/?-flexbox-flexwrap-setclasses !*/
    !function(e,n,t){function r(e,n){return typeof e===n}function o(){var e,n,t,o,s,i,l;for(var a in w)if(w.hasOwnProperty(a)){if(e=[],n=w[a],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(o=r(n.fn,"function")?n.fn():n.fn,s=0;s<e.length;s++)i=e[s],l=i.split("."),1===l.length?Modernizr[l[0]]=o:(!Modernizr[l[0]]||Modernizr[l[0]]instanceof Boolean||(Modernizr[l[0]]=new Boolean(Modernizr[l[0]])),Modernizr[l[0]][l[1]]=o),C.push((o?"":"no-")+l.join("-"))}}function s(e){var n=S.className,t=Modernizr._config.classPrefix||"";if(_&&(n=n.baseVal),Modernizr._config.enableJSClass){var r=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(r,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),_?S.className.baseVal=n:S.className=n)}function i(e,n){return function(){return e.apply(n,arguments)}}function l(e,n,t){var o;for(var s in e)if(e[s]in n)return t===!1?e[s]:(o=n[e[s]],r(o,"function")?i(o,t||n):o);return!1}function a(e,n){return!!~(""+e).indexOf(n)}function f(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):_?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function u(e){return e.replace(/([a-z])-([a-z])/g,function(e,n,t){return n+t.toUpperCase()}).replace(/^-/,"")}function p(e){return e.replace(/([A-Z])/g,function(e,n){return"-"+n.toLowerCase()}).replace(/^ms-/,"-ms-")}function c(n,t,r){var o;if("getComputedStyle"in e){o=getComputedStyle.call(e,n,t);var s=e.console;if(null!==o)r&&(o=o.getPropertyValue(r));else if(s){var i=s.error?"error":"log";s[i].call(s,"getComputedStyle returning null, its possible modernizr test results are inaccurate")}}else o=!t&&n.currentStyle&&n.currentStyle[r];return o}function d(){var e=n.body;return e||(e=f(_?"svg":"body"),e.fake=!0),e}function m(e,t,r,o){var s,i,l,a,u="modernizr",p=f("div"),c=d();if(parseInt(r,10))for(;r--;)l=f("div"),l.id=o?o[r]:u+(r+1),p.appendChild(l);return s=f("style"),s.type="text/css",s.id="s"+u,(c.fake?c:p).appendChild(s),c.appendChild(p),s.styleSheet?s.styleSheet.cssText=e:s.appendChild(n.createTextNode(e)),p.id=u,c.fake&&(c.style.background="",c.style.overflow="hidden",a=S.style.overflow,S.style.overflow="hidden",S.appendChild(c)),i=t(p,e),c.fake?(c.parentNode.removeChild(c),S.style.overflow=a,S.offsetHeight):p.parentNode.removeChild(p),!!i}function y(n,r){var o=n.length;if("CSS"in e&&"supports"in e.CSS){for(;o--;)if(e.CSS.supports(p(n[o]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var s=[];o--;)s.push("("+p(n[o])+":"+r+")");return s=s.join(" or "),m("@supports ("+s+") { #modernizr { position: absolute; } }",function(e){return"absolute"==c(e,null,"position")})}return t}function v(e,n,o,s){function i(){p&&(delete N.style,delete N.modElem)}if(s=r(s,"undefined")?!1:s,!r(o,"undefined")){var l=y(e,o);if(!r(l,"undefined"))return l}for(var p,c,d,m,v,g=["modernizr","tspan","samp"];!N.style&&g.length;)p=!0,N.modElem=f(g.shift()),N.style=N.modElem.style;for(d=e.length,c=0;d>c;c++)if(m=e[c],v=N.style[m],a(m,"-")&&(m=u(m)),N.style[m]!==t){if(s||r(o,"undefined"))return i(),"pfx"==n?m:!0;try{N.style[m]=o}catch(h){}if(N.style[m]!=v)return i(),"pfx"==n?m:!0}return i(),!1}function g(e,n,t,o,s){var i=e.charAt(0).toUpperCase()+e.slice(1),a=(e+" "+P.join(i+" ")+i).split(" ");return r(n,"string")||r(n,"undefined")?v(a,n,o,s):(a=(e+" "+z.join(i+" ")+i).split(" "),l(a,n,t))}function h(e,n,r){return g(e,t,t,n,r)}var C=[],w=[],x={_version:"3.6.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){w.push({name:e,fn:n,options:t})},addAsyncTest:function(e){w.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=x,Modernizr=new Modernizr;var S=n.documentElement,_="svg"===S.nodeName.toLowerCase(),b="Moz O ms Webkit",P=x._config.usePrefixes?b.split(" "):[];x._cssomPrefixes=P;var z=x._config.usePrefixes?b.toLowerCase().split(" "):[];x._domPrefixes=z;var E={elem:f("modernizr")};Modernizr._q.push(function(){delete E.elem});var N={style:E.elem.style};Modernizr._q.unshift(function(){delete N.style}),x.testAllProps=g,x.testAllProps=h,Modernizr.addTest("flexbox",h("flexBasis","1px",!0)),Modernizr.addTest("flexwrap",h("flexWrap","wrap",!0)),o(),s(C),delete x.addTest,delete x.addAsyncTest;for(var T=0;T<Modernizr._q.length;T++)Modernizr._q[T]();e.Modernizr=Modernizr}(window,document);
  
    var html = document.querySelector('.no-flexbox');
  
    if (typeof(html) != 'undefined' && html != null) {
    // if (true) {
      function setStyles(el, selector) {
        switch (selector) {
          case 'unsupported' :
              el.style.backgroundColor = '#ccc';
              el.style.boxSizing = 'border-box';
              el.style.color = '#333';
              el.style.left = '0';
              el.style.fontFamily = 'Arial, sans-serif';
              el.style.height = window.innerHeight+'px';
              el.style.overflow = 'auto';
              el.style.padding = '40px 20px 20px 20px';
              el.style.position = 'absolute';
              el.style.textAlign = 'center';
              el.style.top = '0';
              el.style.width = '100%';
              el.style.zIndex = 99999;
            break;
          case 'unsupported-container' :
              el.style.borderRadius = '5px';
              el.style.boxShadow = '0 5px 10px rgba(0, 0, 0, .2)';
              el.style.margin = 'auto';
              el.style.maxWidth = '700px';
              el.style.overflow = 'hidden';
            break;
          case 'unsupported-container-top' :
            el.style.backgroundColor = '#fff';
            el.style.padding = '60px 40px 60px 40px';
            break;
          case 'unsupported-container-bottom' :
            el.style.backgroundColor = '#f9f9f9';
            el.style.padding = '40px 40px 0 40px';
            break;
          case 'title' :
            el.style.display = 'inline-block';
            el.style.fontSize = '3em';
            el.style.fontWeight = 'bold';
            el.style.lineHeight = '1.2';
            el.style.marginBottom = '20px';
            el.style.width = '100%';
            break;
          case 'p' :
            el.style.color = '#666';
            el.style.fontSize = '14px';
            el.style.lineHeight = '1.5';
            el.style.marginLeft = 'auto';
            el.style.marginRight = 'auto';
            el.style.maxWidth = '500px';
            break;
          case 'ul' :
            el.style.color = '#f9f9f9';
            el.style.listStyleType = 'none';
            el.style.margin = '0';
            el.style.padding = '0';
            el.style.textAlign = 'center';
            el.style.verticalAlign = 'top';
            break;
          case 'li' :
            el.style.display = 'inline-block';
            el.style.fontSize = '14px';
            el.style.listStyleType = 'none';
            el.style.margin = '0 20px 0 20px';
            el.style.verticalAlign = 'top';
            el.style.overflow = 'hidden';
            el.style.paddingBottom = '40px';
            el.style.width = '80px';
            break;
          case 'browser-name' :
            el.style.display = 'inline-block';
            el.style.width = '100%';
            el.style.color = '#333';
            el.style.fontWeight = 'bold';
            el.style.marginBottom = '20px';
            break;
          case 'download' :
            el.style.backgroundColor = '#0686E4';
            el.style.borderRadius = '3px';
            el.style.boxSizing = 'border-box';
            el.style.color = '#fff';
            el.style.fontSize = '12px';
            el.style.fontWeight = '400';
            el.style.padding = '5px 5px 5px 5px';
            el.style.textDecoration = 'none';
            el.style.width = '100%'
            break;
          case 'browser-logo' :
            el.style.height = '40px';
            el.style.margin = '0 auto 10px auto';
            el.style.width = '40px';
            break;
          case 'img' :
            el.style.display = 'block';
            el.style.height = 'auto';
            el.style.width = 'auto';
            el.style.maxHeight = '40px';
            el.style.maxWidth = '40px';
            el.style.width = 'auto';
            break;
          default:
        }
      }
      
      // JS only version of markup / css in https://codepen.io/mh7/pen/zYYJJQG
      var unsupported = document.createElement('div');
      setStyles(unsupported, 'unsupported');
      
      var unsupportedContainer = document.createElement('div');
      setStyles(unsupportedContainer, 'unsupported-container');
      
      var unsupportedContainerTop = document.createElement('div');
      setStyles(unsupportedContainerTop, 'unsupported-container-top');
      
      var unsupportedContainerBottom = document.createElement('div');
      setStyles(unsupportedContainerBottom, 'unsupported-container-bottom');
      
      var title = document.createElement('span');
      setStyles(title, 'title');
      var titleContent = document.createTextNode("Unsupported browser");
      title.appendChild(titleContent);
     
      var paragraph1 = document.createElement('p');
      setStyles(paragraph1, 'p');
      var paragraph1Content = document.createTextNode("This website has been built using the latest web technology. Unfortunately, your browser doesn't support all of these features.");
      paragraph1.appendChild(paragraph1Content);
     
      var paragraph2 = document.createElement('p');
      setStyles(paragraph2, 'p');
      paragraph2.style.marginBottom = '0';
      var paragraph2Content = document.createTextNode("Please download the latest version of one of the browsers below to view this website.");
      paragraph2.appendChild(paragraph2Content);
      
      var ul = document.createElement('ul');
      setStyles(ul, 'ul');
      
      // Chrome
      var chromeListItem = document.createElement('li');
      var chromeLogoContainer = document.createElement('div');
      var chromeLogo = document.createElement('IMG');
      var chromeTitle = document.createElement('span');
      var chromeTitleContent = document.createTextNode('Chrome');
      var chromeButton = document.createElement('a');
      var chromeButtonContent = document.createTextNode('Download');
      
      setStyles(chromeListItem, 'li');
      setStyles(chromeLogoContainer, 'browser-logo');
      setStyles(chromeLogo, 'img');
      setStyles(chromeTitle, 'browser-name');
      setStyles(chromeButton, 'download');
      
      chromeLogo.setAttribute('src', 'https://upload.wikimedia.org/wikipedia/commons/a/a5/Google_Chrome_icon_%28September_2014%29.svg');
      chromeTitle.appendChild(chromeTitleContent);
      chromeButton.setAttribute('href', 'https://www.google.com/chrome/');
      chromeButton.setAttribute('target', '_blank');
      chromeButton.appendChild(chromeButtonContent);
      
      chromeLogoContainer.appendChild(chromeLogo);
      chromeListItem.appendChild(chromeLogoContainer);
      chromeListItem.appendChild(chromeTitle);
      chromeListItem.appendChild(chromeButton);
      
      // Firefox
      var firefoxListItem = document.createElement('li');
      var firefoxLogoContainer = document.createElement('div');
      var firefoxLogo = document.createElement('IMG');
      var firefoxTitle = document.createElement('span');
      var firefoxTitleContent = document.createTextNode('Firefox');
      var firefoxButton = document.createElement('a');
      var firefoxButtonContent = document.createTextNode('Download');
      
      setStyles(firefoxListItem, 'li');
      setStyles(firefoxLogoContainer, 'browser-logo');
      setStyles(firefoxLogo, 'img');
      setStyles(firefoxTitle, 'browser-name');
      setStyles(firefoxButton, 'download');
      
      firefoxLogo.setAttribute('src', 'https://upload.wikimedia.org/wikipedia/commons/6/67/Firefox_Logo%2C_2017.svg');
      firefoxTitle.appendChild(firefoxTitleContent);
      firefoxButton.setAttribute('href', 'https://www.mozilla.org/en-GB/firefox/new/');
      firefoxButton.setAttribute('target', '_blank');
      firefoxButton.appendChild(firefoxButtonContent);
      
      firefoxLogoContainer.appendChild(firefoxLogo);
      firefoxListItem.appendChild(firefoxLogoContainer);
      firefoxListItem.appendChild(firefoxTitle);
      firefoxListItem.appendChild(firefoxButton);
      
      // Safari
      var safariListItem = document.createElement('li');
      var safariLogoContainer = document.createElement('div');
      var safariLogo = document.createElement('IMG');
      var safariTitle = document.createElement('span');
      var safariTitleContent = document.createTextNode('Safari');
      var safariButton = document.createElement('a');
      var safariButtonContent = document.createTextNode('Download');
      
      setStyles(safariListItem, 'li');
      setStyles(safariLogoContainer, 'browser-logo');
      setStyles(safariLogo, 'img');
      setStyles(safariTitle, 'browser-name');
      setStyles(safariButton, 'download');
      
      safariLogo.setAttribute('src', 'https://upload.wikimedia.org/wikipedia/commons/5/52/Safari_browser_logo.svg');
      safariTitle.appendChild(safariTitleContent);
      safariButton.setAttribute('href', 'https://support.apple.com/en_GB/downloads/safari');
      safariButton.setAttribute('target', '_blank');
      safariButton.appendChild(safariButtonContent);
      
      safariLogoContainer.appendChild(safariLogo);
      safariListItem.appendChild(safariLogoContainer);
      safariListItem.appendChild(safariTitle);
      safariListItem.appendChild(safariButton);
      
      // Edge
      var edgeListItem = document.createElement('li');
      var edgeLogoContainer = document.createElement('div');
      var edgeLogo = document.createElement('IMG');
      var edgeTitle = document.createElement('span');
      var edgeTitleContent = document.createTextNode('Edge');
      var edgeButton = document.createElement('a');
      var edgeButtonContent = document.createTextNode('Download');
      
      setStyles(edgeListItem, 'li');
      setStyles(edgeLogoContainer, 'browser-logo');
      setStyles(edgeLogo, 'img');
      setStyles(edgeTitle, 'browser-name');
      setStyles(edgeButton, 'download');
      
      edgeLogo.setAttribute('src', 'https://upload.wikimedia.org/wikipedia/commons/d/d6/Microsoft_Edge_logo.svg');
      edgeTitle.appendChild(edgeTitleContent);
      edgeButton.setAttribute('href', 'https://www.microsoft.com/en-gb/windows/microsoft-edge');
      edgeButton.setAttribute('target', '_blank');
      edgeButton.appendChild(edgeButtonContent);
      
      edgeLogoContainer.appendChild(edgeLogo);
      edgeListItem.appendChild(edgeLogoContainer);
      edgeListItem.appendChild(edgeTitle);
      edgeListItem.appendChild(edgeButton);
      
      // Internet Explorer
      var ieListItem = document.createElement('li');
      var ieLogoContainer = document.createElement('div');
      var ieLogo = document.createElement('IMG');
      var ieTitle = document.createElement('span');
      var ieTitleContent = document.createTextNode('IE11');
      var ieButton = document.createElement('a');
      var ieButtonContent = document.createTextNode('Download');
      
      setStyles(ieListItem, 'li');
      setStyles(ieLogoContainer, 'browser-logo');
      setStyles(ieLogo, 'img');
      setStyles(ieTitle, 'browser-name');
      setStyles(ieButton, 'download');
      
      ieLogo.setAttribute('src', 'https://upload.wikimedia.org/wikipedia/commons/1/18/Internet_Explorer_10%2B11_logo.svg');
      ieTitle.appendChild(ieTitleContent);
      ieButton.setAttribute('href', 'https://www.microsoft.com/en-gb/download/internet-explorer-11-for-windows-7-details.aspx');
      ieButton.setAttribute('target', '_blank');
      ieButton.appendChild(ieButtonContent);
      
      ieLogoContainer.appendChild(ieLogo);
      ieListItem.appendChild(ieLogoContainer);
      ieListItem.appendChild(ieTitle);
      ieListItem.appendChild(ieButton);
      
      ul.appendChild(chromeListItem);
      ul.appendChild(firefoxListItem);
      ul.appendChild(safariListItem);
      ul.appendChild(edgeListItem);
      ul.appendChild(ieListItem);
      
      unsupportedContainerTop.appendChild(title);
      unsupportedContainerTop.appendChild(paragraph1);
      unsupportedContainerTop.appendChild(paragraph2);
      unsupportedContainerBottom.appendChild(ul);
      unsupportedContainer.appendChild(unsupportedContainerTop);
      unsupportedContainer.appendChild(unsupportedContainerBottom);
      unsupported.appendChild(unsupportedContainer);
      document.querySelector('body').appendChild(unsupported);
  
      if (window.innerWidth > 768) {
        document.querySelector('html').style.overflow = 'hidden';
      }
    }
    
  });