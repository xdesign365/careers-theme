document.addEventListener("DOMContentLoaded", () => {
  // Navigation ------------ //
  const navigationButton = document.querySelector('.header__button');
  const header = document.querySelector('.header__container');
  let lastScroll = 0;

  const attachNavigationEvents = () => {
    navigationButton.addEventListener('click', menuOpenHandler);
    window.addEventListener('resize', hideOnResize);
    window.addEventListener('scroll', hideOnScroll);
  }

  const menuOpenHandler = () => {
    const body = document.body;

    if(body.classList.contains('menu-open')) {
      body.classList.add('menu-closing')
      body.classList.remove('menu-open')

      setTimeout(() => {
        body.classList.remove('menu-closing')
      }, 1000);
    } else {
      body.classList.add('menu-open')
    }
  }

  const hideOnResize = () => {
    if(window.innerWidth > 767) {
      document.body.classList.remove('menu-open')
    }
  }

  const hideOnScroll = () => {
    const currentScroll = window.pageYOffset,
          body = document.body,
          headerHeight = header.offsetHeight;

    if(currentScroll > headerHeight) {
      if (currentScroll <= 0) {
        document.body.classList.remove('scroll-up');
        return;
      }
    
      if (currentScroll > lastScroll && !body.classList.contains('scroll-down')) {
        body.classList.remove('scroll-up');
        body.classList.add('scroll-down');
      } else if (currentScroll < lastScroll && body.classList.contains('scroll-down')) {
        body.classList.remove('scroll-down');
        body.classList.add('scroll-up');
      }
    }

    lastScroll = currentScroll;
  }

  // Navigation END ------------ //

    //Animated gradient headings START ------------//
    const headings = document.querySelectorAll('.animated-gradient');
    const options = { threshold: 1.0 }
  
    const observer = new IntersectionObserver((entries) => {
      entries.forEach(entry => {
        const { isIntersecting, target } = entry
  
        if(isIntersecting) {
          target.classList.add('active-gradient')
        } else {
          target.classList.remove('active-gradient')
        }
      })
    }, options)
  
    headings.forEach(heading => {
      observer.observe(heading)
    })
  
    //Animated gradient headings END ------------//

  const SR = ScrollReveal();

  // Scroll reveal animations
  SR.reveal('.load-hidden', { distance: '30px', duration: 600, delay: 125, reset: true });
  SR.reveal('.load-hidden-no-reset', { distance: '30px', duration: 600, delay: 125 });
  SR.reveal('.reveal-left', { distance: '100px', origin: 'left', duration: 750, delay: 250, reset: true });
  SR.reveal('.reveal-right', { distance: '100px', origin: 'right', duration: 750, delay: 250, reset: true });
  SR.reveal('.custom-load-hidden-delay-125', { distance: '30px', delay: 125, duration: 600, reset: true });
  SR.reveal('.custom-load-hidden-delay-250', { distance: '30px', delay: 250, duration: 600, reset: true });
  SR.reveal('.custom-load-hidden-delay-375', { distance: '30px', delay: 375, duration: 600, reset: true });
  SR.reveal('.custom-load-hidden-delay-500', { distance: '30px', delay: 500, duration: 600, reset: true });
  SR.reveal('.custom-load-hidden-delay-625', { distance: '30px', delay: 625, duration: 600, reset: true });
  SR.reveal('.custom-load-hidden-delay-750', { distance: '30px', delay: 750, duration: 600, reset: true });
  SR.reveal('.custom-load-hidden-delay-875', { distance: '30px', delay: 875, duration: 600, reset: true });
  SR.reveal('.custom-load-hidden-delay-1000', { distance: '30px', delay: 1000, duration: 600, reset: true });
  SR.reveal('.custom-load-hidden-delay-1125', { distance: '30px', delay: 1125, duration: 600, reset: true });

  // Set up countupjs events with ScrollTrigger
  document.querySelectorAll('.countupjs').forEach((instance) => {
    let valueElement = instance.querySelector('.value');
    let value = parseInt(valueElement.getAttribute('data-value'));

    valueElement.innerHTML = '0';

    let counter = new countUp.CountUp(valueElement, 0);

    ScrollTrigger.create({
      trigger: instance,
      onToggle: (self) => {
        if (self.isActive) {
          setTimeout(() => {
            counter.update(value);
          }, 125);
        } else {
          counter.update(0);
        }
      }
    })
  });

  // Add scroll-controlled horizontal scrolling through image-slider (will only currently work on first instance)
  if (document.querySelector('.image-slider')) {
    let sections = gsap.utils.toArray('.image-slider__row');
  
    gsap.to(sections, {
      xPercent: -5,
      ease: "none",
      scrollTrigger: {
        trigger: ".image-slider",
        scrub: 1,
        // base vertical scrolling on how wide the container is so it feels more natural.
        end: "+=3500",
      }
    });
  }

    // Add scroll-controlled horizontal scrolling logos block
    if (document.querySelector('.partners') && window.innerWidth > 767) {
      let logosRow = gsap.utils.toArray('.partners__desktop-logos');
    
      gsap.to(logosRow, {
        xPercent: -150,
        ease: "none",
        scrollTrigger: {
          trigger: ".partners",
          scrub: 1,
          // base vertical scrolling on how wide the container is so it feels more natural.
          end: "+=3000",
        }
      });
    }

  // Add parallax scrolling effect to stories-blocks elements
  if (document.querySelector('.stories-blocks')) {
    document.querySelectorAll('.stories-blocks').forEach((storiesBlock) => {
      storiesBlock.querySelectorAll('.stories-blocks__column').forEach((block) => {
        
        gsap.to(block, {
          yPercent: window.innerWidth > 1025 ? block.classList.contains('odd') ? 5 : -30 : 0,
          ease: "none",
          scrollTrigger: {
            trigger: storiesBlock,
            scrub: true
          },
        });

      });
    });
  }

  // Tabs block ------------- //
  const activeBlockHeadings = document.querySelectorAll('.block-tabs__heading');

  activeBlockHeadings.forEach(heading => {
    heading.addEventListener('click', (e) => getActiveIndex(e))
  });
  
  const getActiveIndex = (e) => {
    const { target } = e,
          index = target.dataset.activeElement;

    activeBlockStateHandler(index);
  }

  const activeBlockStateHandler = (index) => {
    const oldActiveElements = document.querySelectorAll('[data-active-element].active'),
          newActiveElements = document.querySelectorAll(`[data-active-element='${index}']`);

    oldActiveElements.forEach(el => el.classList.remove('active'))
    newActiveElements.forEach(el => el.classList.add('active'))
  }


  // Tabs block END ------------- //

  // Accordions
  const accordions = Array.from(document.querySelectorAll('.accordion'));

  accordions.forEach(accordion => accordion.addEventListener('click', (e) => toggleAccordion(e)))

  const toggleAccordion = (e) => {
    const accordion = e.target,
          { lastElementChild } = accordion;

    accordion.classList.toggle('accordion--active');

    if (lastElementChild.style.maxHeight) {
      lastElementChild.style.maxHeight = null;
    } else {
      lastElementChild.style.maxHeight = lastElementChild.scrollHeight + "px";
    }
  }

  // Setting accordion for keyboard

  const accordion = document.querySelector('.accordions-section');

  if(accordion != null) {

    // Lenght of the accordion.
    const accordionNumber = accordions.length;

    // Checker to run function checkFocus() once only.
    let checker = true;
    // Current index of focused element in accordion.
    let currentFocus = null;

      
    // Enter event
    document.addEventListener('keydown', function(event) {
      if(event.which === 13 && document.activeElement.classList.contains('accordion')) {
        
        document.activeElement.classList.toggle('accordion--active');

        if (document.activeElement.lastElementChild.style.maxHeight) {
          document.activeElement.lastElementChild.style.maxHeight = null;
        } else {
          document.activeElement.lastElementChild.style.maxHeight = document.activeElement.lastElementChild.scrollHeight + "px";
        }
      }
    })

    // Right arrow event
    document.addEventListener("keydown", function(event) {
        if(event.which === 39) {

          // function checkFocus()
          if(checker) {
            function checkFocus() {
              accordions.forEach((elem) => {
                if(document.activeElement !== elem) {
                  accordions[0].focus();
                }
              })
            }
            checkFocus();
            checker = false;
          }
          else {
            accordions.forEach((elem, index) => {
              if(document.activeElement == elem) {
                currentFocus = index;
              }
            })
            if(currentFocus < (accordionNumber - 1)) {
              currentFocus++;
              accordions[currentFocus].focus();
            }
            else {
              currentFocus = 0;
              accordions[currentFocus].focus();
            }
          }
        }
    })
    // Left arrow event
    document.addEventListener("keydown", function(event) {
      if(event.which === 37) {

        // function checkFocus()
        if(checker) {
          function checkFocus() {
            accordions.forEach((elem) => {
              if(document.activeElement !== elem) {
                accordions[0].focus();
              }
            })
          }
          checkFocus();
          checker = false;
        }
        else {
          accordions.forEach((elem, index) => {
            if(document.activeElement == elem) {
              currentFocus = index;
            }
          })
          if(currentFocus > 0) {
            currentFocus--;
            accordions[currentFocus].focus();
          }
          else {
            currentFocus = (accordionNumber - 1);
            accordions[currentFocus].focus();
          }
        }
      }
  })
  }
  // Accordion END

  /**
  * Collection of functions to run on page load
  */
  const init = () => {
    attachNavigationEvents();
  }

  init();
});
