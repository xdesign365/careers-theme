(() => {


    // Domains to append UTM links to
    let domains = [
        window.location.hostname,
        'https://apply.workable.com/',
    ];

    // Potential UTM parameters that may be parsed
    let queryParameters = [
        'utm_medium',
        'utm_source',
        'utm_campaign',
        'utm_id',
        'utm_term',
    ];

    const getCookie = (name) => {
        // Split cookie string and get all individual name=value pairs in an array
        let cookieArray = document.cookie.split(";");
        let foundCookieValue = null;
        
        // Loop through the array elements
        cookieArray.forEach((cookie) => {
            let cookiePair = cookie.split('=');
            let cookieString = null;

            if (name == cookiePair[0] || ` ${name}` == cookiePair[0]) {
                foundCookieValue = decodeURIComponent(cookiePair[1]);
            }
        });
        
        // Return null if not found
        return foundCookieValue;
    }

    const decorateURL = (url) => {
        let collectedQueryParameters = [];

        url = (url.indexOf('?') === -1) ? url + '?' : url + '&';

        queryParameters.forEach((queryParameter) => {
            if (getQueryParameterFromURL(queryParameter)) {
                collectedQueryParameters.push(queryParameter + '=' + getQueryParameterFromURL(queryParameter))
            }
        });

        return url + collectedQueryParameters.join('&');
    };

    const getQueryParameterFromURL = (name) => {
        let fakeWindowObject = window.location;
        let fakeURL = null;
        let search = null;

        if (navigator.cookieEnabled) {

            if (getCookie('utm') !== null) {
                search = getCookie('utm');
            } else {
                if (fakeWindowObject.href.indexOf('#') !== -1) {
                    fakeURL = `${ fakeWindowObject.href.substr(0, fakeWindowObject.href.indexOf('#')) }${ fakeWindowObject.href.substr(fakeWindowObject.href.indexOf('?'), fakeWindowObject.href.length) }`;
                    search = fakeURL.substr(fakeURL.indexOf('?'), fakeURL.length);
                } else {
                    search = fakeWindowObject.search;
                }
            }
        } else {
            if (fakeWindowObject.href.indexOf('#') !== -1) {
                fakeURL = `${ fakeWindowObject.href.substr(0, fakeWindowObject.href.indexOf('#')) }${ fakeWindowObject.href.substr(fakeWindowObject.href.indexOf('?'), fakeWindowObject.href.length) }`;
                search = fakeURL.substr(fakeURL.indexOf('?'), fakeURL.length);
            } else {
                search = fakeWindowObject.search;
            }
        }

        if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(search)) {
            return decodeURIComponent(name[1]);
        }
    };

    window.updateLinks = () => {
        
        if ((window.location.href.indexOf('?') !== -1) || document.cookie.indexOf('utm') !== -1) {
            document.querySelectorAll('a').forEach((link) => {

                if (link.hasAttribute('utm-bundled') && link.getAttribute('utm-bundled') == 'true') {
                    return;
                } else {
                    link.setAttribute('utm-bundled', 'true');
                }
    
                domains.forEach((domain) => {
                    // Check if domain exists in link AND domain doesn't include #
                    if ((link.href.indexOf(domain) > -1) && (link.href.indexOf('#') === -1)) {
                        link.href = decorateURL(link.href);
                    // Check if domain exists AND domain does include #
                    } else if ((link.href.indexOf(domain) > -1) && (link.href.indexOf('#') !== -1)) {
                        link.href = `${ link.href.substr(0, link.href.indexOf('?')) }${ link.href.substr(link.href.indexOf('#'), link.href.length) }`;
                        link.href = decorateURL(link.href);
                    }
                });
            });
        
            if (navigator.cookieEnabled) {
                if (window.location.href.indexOf('?') !== -1) {
                    document.cookie = `utm=${ encodeURIComponent(window.location.href.substr(window.location.href.indexOf('?'), window.location.href.length)) }`;
                }
            }
        }
    }

    window.updateLinks();

})();
