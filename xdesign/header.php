<?php 
	$navigaton = get_field('navigation', 'options') ;
    $delayIndex = 0;
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8" />
	<meta name="format-detection" content="telephone=no" />
	<meta http-equiv="Content-Language" content="en">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/wp-content/themes/xdesign/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="a/wp-content/themes/xdesign/favicon/pple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/wp-content/themes/xdesign/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/wp-content/themes/xdesign/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="/wp-content/themes/xdesign/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/wp-content/themes/xdesign/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/wp-content/themes/xdesign/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/wp-content/themes/xdesign/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/wp-content/themes/xdesign/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="/wp-content/themes/xdesign/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="/wp-content/themes/xdesign/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/wp-content/themes/xdesign/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="/wp-content/themes/xdesign/favicon/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="xDesign Careers"/>
	<meta name="msapplication-TileImage" content="/wp-content/themes/xdesign/favicon/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="/wp-content/themes/xdesign/favicon/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="/wp-content/themes/xdesign/favicon/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="/wp-content/themes/xdesign/favicon/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="/wp-content/themes/xdesign/favicon/mstile-310x310.png" />
	<meta name="msapplication-TileColor" content="#090920">
	<meta name="theme-color" content="#090920">
	<script src="https://unpkg.com/scrollreveal"></script>
	<title><?php echo wp_title(); ?></title>
	<link rel="preconnect" href="https://script.hotjar.com">
	<link rel="preconnect" href="https://static.hotjar.com">
	<link rel="preconnect" href="https://www.google-analytics.com">
	<link rel="preconnect" href="https://cookiehub.net">
	<link rel="preconnect" href="https://js.hs-banner.com">
	<link rel="preconnect" href="https://js.hs-scripts.com">
	<link rel="manifest" href="/wp-content/themes/xdesign/manifest.json" />
	<script>
  if (window.screen.availWidth < 600) {
	document.currentScript.insertAdjacentHTML(
		'beforebegin', 
		'<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">'
	)
  } else {
	document.currentScript.insertAdjacentHTML(
		'beforebegin', 
		'<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">'
	)
  }
</script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header class="header">

		<div class="header__container">
			<a href="/" aria-label="xDesign homepage">
				<img width="100%" src="<?php echo $navigaton['logo'] ;?>" alt="xDesign Careers" />
			</a>
			<div class="header__button">
				<span class="header__button-line"></span>
				<span class="header__button-line"></span>
				<span class="header__button-line"></span>
			</div>
<nav class="navigation">
				<ul class="navigation__container">
					            							<li class="navigation__item">
								<a href="https://careers.xdesign.com/open-roles/" class="navigation__link">
									Open Roles								</a>
							</li>
            							<li class="navigation__item">
								<a href="https://careers.xdesign.com/our-story/" class="navigation__link">
									Our Story								</a>
							</li>
            							<li class="navigation__item">
								<a href="https://careers.xdesign.com/life-at-xdesign/" class="navigation__link">
									Life at xDesign								</a>
							</li>
            							<li class="navigation__item">
								<a href="https://careers.xdesign.com/benefits/" class="navigation__link">
									Benefits								</a>
							</li>
            							<li class="navigation__item">
                          <a rel="noopener" target="" href="https://careers.xdesign.com/graduate-programme/" class="navigation__link">

Graduates </a>
							</li>
                      				</ul>
			</nav>
		</div>

		<nav class="menu" style="background-image: url('https://careers.xdesign.com/wp-content/uploads/2021/12/menu-background.png')">
			<div class="menu__background-filter"></div>
			<ul class="menu__container">
															<li class="menu__item animation-delay-0">
							<a href="/open-roles/" class="menu__link">
								Open Roles								<span class="menu__link-icon">
									<svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M18.6457 12.6566L0.131234 12.6566L0.131235 9.34337L18.6457 9.34337L12.9767 2.12048L15.3735 -6.37013e-07L24 11.0066L15.3735 22L12.9767 19.8795L18.6457 12.6566Z" fill="currentColor"></path>
									</svg>
								</span>
							</a>
						</li>
											<li class="menu__item animation-delay-1">
							<a href="/our-story/ " class="menu__link">
								Our Story								<span class="menu__link-icon">
									<svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M18.6457 12.6566L0.131234 12.6566L0.131235 9.34337L18.6457 9.34337L12.9767 2.12048L15.3735 -6.37013e-07L24 11.0066L15.3735 22L12.9767 19.8795L18.6457 12.6566Z" fill="currentColor"></path>
									</svg>
								</span>
							</a>
						</li>
											<li class="menu__item animation-delay-2">
							<a href="/life-at-xdesign/ " class="menu__link">
								Life at xDesign								<span class="menu__link-icon">
									<svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M18.6457 12.6566L0.131234 12.6566L0.131235 9.34337L18.6457 9.34337L12.9767 2.12048L15.3735 -6.37013e-07L24 11.0066L15.3735 22L12.9767 19.8795L18.6457 12.6566Z" fill="currentColor"></path>
									</svg>
								</span>
							</a>
						</li>
											<li class="menu__item animation-delay-3">
							<a href="/benefits/" class="menu__link">
								Benefits								<span class="menu__link-icon">
									<svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M18.6457 12.6566L0.131234 12.6566L0.131235 9.34337L18.6457 9.34337L12.9767 2.12048L15.3735 -6.37013e-07L24 11.0066L15.3735 22L12.9767 19.8795L18.6457 12.6566Z" fill="currentColor"></path>
									</svg>
								</span>
							</a>
						</li>
											<li class="menu__item animation-delay-5">
							<a href="https://careers.xdesign.com/graduate-programme/" class="menu__link">
								Graduates								<span class="menu__link-icon">
									<svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M18.6457 12.6566L0.131234 12.6566L0.131235 9.34337L18.6457 9.34337L12.9767 2.12048L15.3735 -6.37013e-07L24 11.0066L15.3735 22L12.9767 19.8795L18.6457 12.6566Z" fill="currentColor"></path>
									</svg>
								</span>
							</a>
						</li>
												</ul>
		</nav>

	</header>
