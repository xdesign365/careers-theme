<section class="stories-blocks gradient-<?php echo get_field('title_gradient_color'); ?>">
  <?php if(get_field('background_image')) : ?> 
    <div class="section-bg-image" style="background: linear-gradient(0deg, #090821, #090821), url(<?php echo get_field('background_image'); ?>);"></div>
  <?php endif; ?>

  <div class="container overflow-visible">
  <div class="cols">
    <?php if(get_field('show_section_separator')): ?>
      <div class="col section-separator">
        <div class="section-separator__line"></div>
        <span class="section-separator__text">
          <?php echo get_field('separator_text'); ?>
        </span>
      </div>
    <?php endif ?>

    <div class="col is-12">
      <h2 class="stories-blocks__heading gradient-text load-hidden">
        <?php echo get_field('title');?>
      </h2>
    </div>
  </div>

      <div class="cols">
        <?php $i=1; while(have_rows('blocks')) : the_row(); ?>
          <div class="col is-12 is-6-sm is-4-lg flex stories-blocks__column <?php echo ((($i - 2) % 3) == 0) ? 'odd' : 'even'; ?>">
            <div class="stories-blocks__block load-hidden">
              <p class="stories-blocks__description">
                <?php the_sub_field('description'); ?>
              </p>
              <div class="stories-blocks__info">
                <?php $img = get_sub_field('image'); ?>
                <img class="stories-blocks__image" src="<?php echo $img['sizes']['square-thumb'];?>" alt="<?php echo get_sub_field('image')['alt'];?>">
                <div class="stories-blocks__author-info">
                  <strong><?php echo get_sub_field('author');?></strong><br>
                  <strong><?php echo get_sub_field('author_title');?></strong><br>
                </div>
              </div>
            </div>
          </div>
        <?php $i++; endwhile; ?>
      </div>
    </div>
  </div>

</section>
