<section class="our-team-gallery container">
    <div class="our-team-gallery__wrapper">

    <div class="our-team-gallery__images-top">
        
        <?php
        $counter = 0;
        if( have_rows('image_gallery') ) :
                while( have_rows('image_gallery') ) : the_row();

                    $counter++;

                    if($counter <= 3) {
                        // Loading sub values.
                        $image = get_sub_field('image');
    
                        // Using function to print out
                        echo wp_get_attachment_image( $image, '300' );
                    };

                endwhile;
            endif;
        ?>
    </div>
    <div class="our-team-gallery__main-content">

        <?php
            $main_title = get_field('our_team_main_title');
            $colored_part_main_title = get_field('main_title_colored_part');
            $text_content = get_field('text_content');
        ?>

        <h2 class="our-team-gallery__main-title"><?php echo $main_title;?><span class="gradient-<?php echo get_field('gradient_colors') ?> gradient-text"><?php echo $colored_part_main_title ?></span></h2>
        <p class="our-team-gallery__text-content"><?php echo $text_content; ?></p>

        <?php

            // Checking if it's is image or video.
            $image_or_video = get_field('choose_image_or_video');
            // Main image.
            $main_image = get_field('add_main_image')['url'];
            // Checking what type of upload is for video.
            $video_choices = get_field('video_choices');
            // Embeded video.
            $embeded_video = get_field('embed_video');
            // Uploaded video.
            $uploaded_video = get_field('uploaded_video');

            if($image_or_video === 'Image') {
                ?>
                    <img class="main-thumbnail" src="<?php echo $main_image ?>">
                <?php
            }
            else if ($video_choices === 'Embed') {
                ?>
                    <div class="our-team-gallery__embded-video main-thumbnail">
                        <?php echo $embeded_video ?>
                    </div>
                <?php
            }
            else {

                $args = array(
                    'src'   =>  $uploaded_video,
                    'width' => 540,
                    'height' => 280
                )
                ?>
                    <div class="our-team-gallery__uploaded-video main-thumbnail">
                        <?php echo wp_video_shortcode( $args ) ?>
                    </div>
                <?php
            }

        ?>
       
    </div>
        </div>
    <div class="our-team-gallery__images-bottom">
        
        <?php
        $counter = 0;
        if( have_rows('image_gallery') ) :
                while( have_rows('image_gallery') ) : the_row();

                    $counter++;

                    if($counter > 3) {
                        // Loading sub values.
                        $image = get_sub_field('image');
    
                        // Using function to print out
                        echo wp_get_attachment_image( $image, '300' );
                    };

                endwhile;
            endif;
        ?>
    </div>
</section>
