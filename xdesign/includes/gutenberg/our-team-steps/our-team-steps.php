<?php
  $delayIndex = 0;
  $delays = [250, 500, 750];
?>

<section class="our-story-steps">
  <div class="container">
    <div class="cols">
      <?php
          $divider_text = get_field('divider_text'); 

          if($divider_text) { ?>

          <div class="col is-12 section-separator load-hidden">
            <div class="section-separator__line"></div>
            <span class="section-separator__text load-hidden">
                <?php echo $divider_text; ?>
            </span>
          </div>

          <?php }; 
      ?>
    </div>

    <div class="cols">
      <div class="col is-12 our-story-steps__wrapper">

        <?php
          if( have_rows('steps') ) :
            while( have_rows('steps') ) : the_row();

            // Loading sub values.
            $number = get_sub_field('number');
            $number_text = get_sub_field('number_text');
            $text_content = get_sub_field('text_content');
        ?>

              <div class="our-story-steps__tab custom-load-hidden custom-load-hidden-delay-<?php echo $delays[$delayIndex]; ?>">
                <p class="our-story-steps__number"><?php echo $number ?></p>
                <p class="our-story-steps__number-text sub-heading"><?php echo $number_text; ?></p>
                <p class="our-story-steps__text-content"><?php echo $text_content; ?></p>
              </div>

            <?php
            $delayIndex++;
            endwhile;
          endif;
        ?>

      </div>
    </div>
  </div>
</section>
