<section class="accordions-section gradient-<?php echo get_field('gradient_color');?>">
  <div class="container">
    <div class="cols">

      <div class="col is-12 is-6-md">
        <h2 class="accordions-section__heading load-hidden">
          <span class="gradient-text"><?php echo get_field('gradient_text');?></span>
          <?php echo get_field('title');?>
        </h2>
      </div>

      <div class="col is-12 load-hidden">
        <?php while(have_rows('accordions')): the_row(); ?>
          <div class="accordion">
            <div class="accordion__header">
              <h5>
                <?php echo get_sub_field('title'); ?>
              </h5>

              <div class="accordion__icons">
                <span class="accordion__icon accordion__icon--open">
                  <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12.833 12.8335V8.16683H15.1663V12.8335H19.833V15.1668H15.1663V19.8335H12.833V15.1668H8.16634V12.8335H12.833ZM13.9997 25.6668C7.55618 25.6668 2.33301 20.4437 2.33301 14.0002C2.33301 7.55666 7.55618 2.3335 13.9997 2.3335C20.4432 2.3335 25.6663 7.55666 25.6663 14.0002C25.6663 20.4437 20.4432 25.6668 13.9997 25.6668ZM13.9997 23.3335C16.475 23.3335 18.849 22.3502 20.5993 20.5998C22.3497 18.8495 23.333 16.4755 23.333 14.0002C23.333 11.5248 22.3497 9.15084 20.5993 7.4005C18.849 5.65016 16.475 4.66683 13.9997 4.66683C11.5243 4.66683 9.15035 5.65016 7.40001 7.4005C5.64967 9.15084 4.66634 11.5248 4.66634 14.0002C4.66634 16.4755 5.64967 18.8495 7.40001 20.5998C9.15035 22.3502 11.5243 23.3335 13.9997 23.3335Z" fill="#FAFAFB"/>
                  </svg>
                </span>
  
                <span class="accordion__icon accordion__icon--close">
                  <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.9997 25.6668C7.55618 25.6668 2.33301 20.4437 2.33301 14.0002C2.33301 7.55666 7.55618 2.3335 13.9997 2.3335C20.4432 2.3335 25.6663 7.55666 25.6663 14.0002C25.6663 20.4437 20.4432 25.6668 13.9997 25.6668ZM13.9997 23.3335C16.475 23.3335 18.849 22.3502 20.5993 20.5998C22.3497 18.8495 23.333 16.4755 23.333 14.0002C23.333 11.5248 22.3497 9.15084 20.5993 7.4005C18.849 5.65016 16.475 4.66683 13.9997 4.66683C11.5243 4.66683 9.15035 5.65016 7.40001 7.4005C5.64967 9.15084 4.66634 11.5248 4.66634 14.0002C4.66634 16.4755 5.64967 18.8495 7.40001 20.5998C9.15035 22.3502 11.5243 23.3335 13.9997 23.3335V23.3335ZM8.16634 12.8335H19.833V15.1668H8.16634V12.8335Z" fill="#A371E5"/>
                  </svg>
                </span>
              </div>
            </div>
            <div class="accordion__content">
              <p>
                <?php echo get_sub_field('text'); ?>
              </p>
            </div>
          </div>
        <?php endwhile; ?>
      </div>

    </div>
  </div>
</section>
