
<?php 
        if(function_exists('use_block_editor_for_post') && use_block_editor_for_post($post)){
                echo '<div class="acf-block-fields acf-fields" style="padding: 2em;">';
                echo '<h2>Greenhouse Job board</h2>';
                echo '</div>';
        } else {
                if (class_exists('GreenhouseFeed')) {
                        echo do_shortcode('[GreenhouseFeed]'); 
                }
        }
?>