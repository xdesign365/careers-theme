<?php

function custom_gutenberg_blocks()
{
  // var_dump(function_exists('acf_register_block_type')); die;

  if (function_exists('acf_register_block_type')) :

    //----------------------------
    // Register block: Hero - A
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'hero_a',
      'title'             => __('Hero - A'),
      'description'       => __('Hero A'),
      'render_template'   => 'includes/gutenberg/hero-a/hero-a.php',
      'category'          => 'hero',
      'icon'              => 'align-full-width',
      'keywords'          => array('home', 'hero', 'a'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Hero - B
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'hero_b',
      'title'             => __('Hero - B'),
      'description'       => __('Hero B'),
      'render_template'   => 'includes/gutenberg/hero-b/hero-b.php',
      'category'          => 'hero',
      'icon'              => 'align-full-width',
      'keywords'          => array('home', 'hero', 'b'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Hero - C
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'hero_c',
      'title'             => __('Hero - C'),
      'description'       => __('Hero C'),
      'render_template'   => 'includes/gutenberg/hero-c/hero-c.php',
      'category'          => 'hero',
      'icon'              => 'align-full-width',
      'keywords'          => array('home', 'hero', 'c'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Page Title
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'page_title',
      'title'             => __('Page Title'),
      'description'       => __('Page Title'),
      'render_template'   => 'includes/gutenberg/page-title/page-title.php',
      'category'          => 'title',
      'icon'              => 'align-full-width',
      'keywords'          => array('page', 'title'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Page Title
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'text_section',
      'title'             => __('Text Section'),
      'description'       => __('Text Section'),
      'render_template'   => 'includes/gutenberg/text-section/text-section.php',
      'category'          => 'text',
      'icon'              => 'align-full-width',
      'keywords'          => array('text', 'section', 'wysiwyg'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Text Intro
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'text_intro',
      'title'             => __('Text Intro'),
      'description'       => __('Text Intro paragraph'),
      'render_template'   => 'includes/gutenberg/text-intro/text-intro.php',
      'category'          => 'text',
      'icon'              => 'align-full-width',
      'keywords'          => array('text', 'intro', 'paragraph'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Image Slider
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'image_slider',
      'title'             => __('Image Slider'),
      'description'       => __('Grid slider'),
      'render_template'   => 'includes/gutenberg/image-slider/image-slider.php',
      'category'          => 'slider',
      'icon'              => 'align-full-width',
      'keywords'          => array('slider', 'image', 'grid'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Titled Text Block
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'titled_text_block',
      'title'             => __('Titled Text Block'),
      'description'       => __('Columns with a title.'),
      'render_template'   => 'includes/gutenberg/titled-text-blocks/titled-text-blocks.php',
      'category'          => 'text-blocks',
      'icon'              => 'align-full-width',
      'keywords'          => array('title', 'block', 'columns'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    
    //----------------------------
    // Register block: CTA Section
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'cta_section',
      'title'             => __('CTA Section'),
      'description'       => __('Title and a CTA button.'),
      'render_template'   => 'includes/gutenberg/cta-section/cta-section.php',
      'category'          => 'cta',
      'icon'              => 'align-full-width',
      'keywords'          => array('cta', 'button', 'section'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: CTA Small
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'cta_small',
      'title'             => __('CTA Small'),
      'description'       => __('Title, text and a CTA button.'),
      'render_template'   => 'includes/gutenberg/cta-small/cta-small.php',
      'category'          => 'cta',
      'icon'              => 'align-full-width',
      'keywords'          => array('cta', 'button', 'small'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Active blocks
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'active_blocks',
      'title'             => __('Active Blocks'),
      'description'       => __('Click to reveal text.'),
      'render_template'   => 'includes/gutenberg/block-tabs/block-tabs.php',
      'category'          => 'text-blocks',
      'icon'              => 'align-full-width',
      'keywords'          => array('reveal', 'blocks', 'text'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Titled Text Block (Background Image)
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'titled_text_block_background',
      'title'             => __('Titled Text Block (Background Image)'),
      'description'       => __('Column with a title and a background image.'),
      'render_template'   => 'includes/gutenberg/titled-text-blocks-background/titled-text-blocks-background.php',
      'category'          => 'text-blocks',
      'icon'              => 'align-full-width',
      'keywords'          => array('title', 'block', 'background'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Sticky Anchors ( NEW CREATED BLOCKS )
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'sticky_anchors',
      'title'             => __('Sticky Anchors'),
      'description'       => __('Sticky Anchors'),
      'render_template'   => 'includes/gutenberg/sticky-anchors/sticky-anchors.php',
      'category'          => 'sticky-anchors',
      'icon'              => 'align-full-width',
      'keywords'          => array('sticky anchors'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Stories Blocks
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'stories_blocks',
      'title'             => __('Stories Blocks'),
      'description'       => __('Stories Blocks'),
      'render_template'   => 'includes/gutenberg/stories-blocks/stories-blocks.php',
      'category'          => 'stories-blocks',
      'icon'              => 'align-full-width',
      'keywords'          => array('stories blocks'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

     //----------------------------
    // Register block: Icon Text Tiles
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'icon_text_tiles',
      'title'             => __('Icon Text Tiles'),
      'description'       => __('Icon Text Tiles'),
      'render_template'   => 'includes/gutenberg/icon-text-tiles/icon-text-tiles.php',
      'category'          => 'icon-text-tiles',
      'icon'              => 'align-full-width',
      'keywords'          => array('icon-text-tiles', 'tiles'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Icon Text Tiles
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'rating_tiles',
      'title'             => __('Rating Tiles'),
      'description'       => __('Rating Tiles'),
      'render_template'   => 'includes/gutenberg/rating-tiles/rating-tiles.php',
      'category'          => 'rating-tiles',
      'icon'              => 'align-full-width',
      'keywords'          => array('rating-tiles', 'rating'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Two Columns
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'two_columns',
      'title'             => __('Two Columns'),
      'description'       => __('Two Columns'),
      'render_template'   => 'includes/gutenberg/two-columns/two-columns.php',
      'category'          => 'two-columns',
      'icon'              => 'align-full-width',
      'keywords'          => array('two-columns', 'columns'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));


    //----------------------------
    // Register block: Partners Logos
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'partners_logos',
      'title'             => __('Partners Logos'),
      'description'       => __('Partners Logos'),
      'render_template'   => 'includes/gutenberg/partners-logos/partners-logos.php',
      'category'          => 'partners-logos',
      'icon'              => 'align-full-width',
      'keywords'          => array('partners-logos', 'logos'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Gradient Content Gradient Columns
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'gradient_content_columns',
      'title'             => __('Gradient Content Columns'),
      'description'       => __('Gradient Content Columns'),
      'render_template'   => 'includes/gutenberg/gradient-content-columns/gradient-content-columns.php',
      'category'          => 'gradient-content-columns',
      'icon'              => 'align-full-width',
      'keywords'          => array('gradient-content-columns', 'gradient'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Gradient Content Gradient Columns
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'accordion',
      'title'             => __('Accordion'),
      'description'       => __('Horizontal Accordion'),
      'render_template'   => 'includes/gutenberg/accordions/accordions.php',
      'category'          => 'misc',
      'icon'              => 'align-full-width',
      'keywords'          => array('accordion', 'gradient'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Where We Are
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'latest_blogs',
      'title'             => __('Latest Blogs'),
      'description'       => __('Latest Blogs'),
      'render_template'   => 'includes/gutenberg/latest-blogs/latest-blogs.php',
      'category'          => 'latest-blogs',
      'icon'              => 'align-full-width',
      'keywords'          => array('latest', 'blogs'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Where We Are
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'where_we_are',
      'title'             => __('Where We Are'),
      'description'       => __('Where We Are'),
      'render_template'   => 'includes/gutenberg/where-we-are/where-we-are.php',
      'category'          => 'where-we-are',
      'icon'              => 'align-full-width',
      'keywords'          => array('where', 'location'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));
    //----------------------------
    // Register block: Our Team Image Gallery 
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'our-team-image-gallery',
      'title'             => __('Our Team Image gallery'),
      'description'       => __('Our Team Image gallery'),
      'render_template'   => 'includes/gutenberg/our-team-image-gallery/our-team-image-gallery.php',
      'category'          => 'our-team-image-gallery',
      'icon'              => 'align-full-width',
      'keywords'          => array('team', 'image', 'gallery'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));
    
    //----------------------------
    // Register block: Our Team Persons
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'our-team-persons',
      'title'             => __('Our Team Persons'),
      'description'       => __('Our Team Persons'),
      'render_template'   => 'includes/gutenberg/our-team-persons/our-team-persons.php',
      'category'          => 'our-team-persons',
      'icon'              => 'align-full-width',
      'keywords'          => array('team', 'persons', 'our'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));
    //----------------------------
    // Register block: Our Team Steps
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'our-team-steps',
      'title'             => __('Our Team Steps'),
      'description'       => __('Our Team Steps'),
      'render_template'   => 'includes/gutenberg/our-team-steps/our-team-steps.php',
      'category'          => 'our-team-steps',
      'icon'              => 'align-full-width',
      'keywords'          => array('team', 'steps', 'our'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Workable API
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'workable-api',
      'title'             => __('Workable API'),
      'description'       => __('Workable API'),
      'render_template'   => 'includes/gutenberg/workable-api/workable-api.php',
      'category'          => 'workable-api',
      'icon'              => 'align-full-width',
      'keywords'          => array('workable', 'api', 'careers'),
      'post_types'        => array('page'),
      'mode'              => 'edit',
    ));

    //----------------------------
    // Register block: Greenhouse Job List
    //----------------------------
    acf_register_block_type(array(
      'name'              => 'greenhouse-job-board',
      'title'             => __('Greenhouse Job Board'),
      'description'       => __('Lists jobs and includes filters from greenhouse data'),
      'render_template'   => 'includes/gutenberg/greenhouse-job-board/greenhouse-job-board.php',
      'category'          => 'greenhouse-api',
      'icon'              => 'align-full-width',
      'keywords'          => array('greenhouse', 'api', 'careers'),
      'post_types'        => array('page'),
      'mode'              => 'preview',
    ));

  endif;
}

add_action('acf/init', 'custom_gutenberg_blocks');
