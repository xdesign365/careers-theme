<section class="scrolling-anchors gradient-<?php echo get_field('gradient_colors');?>">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12 is-6-md is-5-lg">
      <div class="sticky-container">
          <?php if(get_field('show_pre-title')): ?>
            <div class="section-separator load-hidden-no-reset">
              <div class="section-separator__line"></div>
              <span class="section-separator__text">
                <?php echo get_field('pre-title'); ?>
              </span>
            </div>
          <?php endif ?>

          <h2 class="scrolling-anchors__title load-hidden-no-reset">
            <?php echo get_field('title');?>
            <span class="gradient-text scrolling-anchors__gradient-text"> <?php echo get_field('gradient_title');?> </span>
          </h2>
        </div>
      </div>
  
      <div class="col is-12 is-offset-3-md is-9-md is-offset-0-lg is-7-lg">
        <?php while(have_rows('section')) : the_row(); ?>
        
        <?php if(!empty(get_sub_field('url') && (strlen(get_sub_field('url')) > 0))) {
          $sticky_url = get_sub_field('url');
        }
        else {
          $sticky_url = '#';
        } ?>

          <a href="<?php echo $sticky_url; ?>" class="scrolling-anchors__anchor load-hidden">
            <div class="scrolling-anchors__header">
              <img class="scrolling-anchors__icon scrolling-anchors__icon--normal style-svg" src="<?php echo get_sub_field('icon')['url']; ?>" alt="<?php echo get_sub_field('icon')['alt'];?>">
              <img class="scrolling-anchors__icon scrolling-anchors__icon--hover style-svg" src="<?php echo get_sub_field('hover_icon')['url']; ?>" alt="<?php echo get_sub_field('hover_icon')['alt'];?>">

              <h3 class="scrolling-anchors__heading">
                <?php echo get_sub_field('title'); ?>
              </h3>

              <?php if(get_sub_field('show_arrow')): ?>
                <span class="scrolling-anchors__icon-wrapper">
                  <svg class="scrolling-anchors__arrow" width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14.8932 10.3328L0.000246826 10.3328L0.000247102 7.66766L14.8932 7.66766L10.3331 1.85757L12.2611 0.151857L19.2002 9.00558L12.2611 17.8486L10.3331 16.1429L14.8932 10.3328Z" fill="currentColor"/>
                  </svg>
                </span>
              <?php endif ?>
            </div>
            <p class="scrolling-anchors__description">
              <?php echo get_sub_field('description'); ?>
            </p>
          </a>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
</section>
