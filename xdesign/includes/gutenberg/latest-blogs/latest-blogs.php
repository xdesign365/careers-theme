<?php
    ?> <div class="latest-blogs container">
        <h2 class="latest-blogs__main-title gradient-text gradient-<?php echo get_field('title_gradient'); ?>"><?php echo get_field('main_title'); ?></h2>      
    <?php





    if( have_rows('latest_blogs') ) : 
    
        ?> <div class="latest-blogs__blogs-wrapper"> <?php

        while( have_rows('latest_blogs') ) : the_row();

            ?>
                <div class="latest-blogs__blog-tab">

                    <div class="latest-blogs__image-wrapper">
                        <?php
                            echo wp_get_attachment_image(get_sub_field('featured_image'), 'full' );
                        ?>
                    </div>

                    <?php 
                        if(get_sub_field('date_options')) {
                            ?>
                                <span class="latest-blogs__date">
                                    <?php
                                        echo get_sub_field('date_posted');
                                    ?>
                                </span>
                            <?php
                        }
                    ?>

                    <h3 class="latest-blogs__blog-title <?php if(!get_sub_field('date_options')) {
                        echo 'no-date-title-fix';
                    } ?>" >
                        <?php echo get_sub_field('title'); ?>
                    </h3>

                    <p class="latest-blogs__blog-text-content">
                        <?php echo get_sub_field('text_content'); ?>
                    </p>

                    <?php 
                        if( get_sub_field('cta_link') ) { ?>

                            <a rel="noopener" href="<?php echo get_sub_field('cta_link')['url']; ?>" target="<?php echo get_sub_field('cta_link')['target']; ?>" class="latest-blogs__cta-link"><?php echo get_sub_field('cta_link')['title']; ?></a>

                        <?php
                        }
                    ?>

                </div>
            
            <?php

        endwhile;

            ?> </div> <?php

    endif;

    ?> </div> <?php
?>