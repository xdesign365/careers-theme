<div class="page-title gradient-<?php echo get_field('gradient_color');?>">
  <div class="container">
    <div class="cols">
      <div class="col is-12 load-hidden">
        <h2 class="page-title__title gradient-text">
          <?php echo get_field('title');?>
        </h2>
      </div>
    </div>
  </div>
</div>
