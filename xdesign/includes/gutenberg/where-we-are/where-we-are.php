<?php
  $delayIndex = 0;
  $delays = [125, 250, 375, 500, 625, 750];
  $numberOfAddresses = count(get_field('address_location_tab'))
?>

<section class='addresses'>
  <div class="container">
    <div class="cols">
      <?php
        $divider_text = get_field('divider_text'); 

        if($divider_text) { ?>

        <div class="col is-12 section-separator load-hidden">
          <div class="section-separator__line"></div>
          <span class="section-separator__text load-hidden">
              <?php echo $divider_text; ?>
          </span>
        </div>

        <?php }; 
      ?>

      <div class="col is-12">
        <?php if (get_field('main_title')) ?>

          <h2 class="gradient-<?php echo get_field('gradient_colors'); ?> addresses__title load-hidden">
            <?php echo get_field('main_title'); ?>
            <span class="highlight-text"><?php echo get_field('main_title_colored_part'); ?></span>
          </h2>

      </div>

      <?php if (have_rows('address_location_tab')) ?>
        <div class="col is-12 addresses__wrapper <?php echo ($numberOfAddresses % 2 == 1)? 'addresses__wrapper--odd-cols' : '';?>">
          <?php while( have_rows('address_location_tab') ) : the_row(); ?>
            <div class="addresses__box custom-load-hidden custom-load-hidden-delay-<?php echo $delays[$delayIndex]; ?>">
              <p class="addresses__town"><?php echo get_sub_field('town_city'); ?></p>
              <p class="addresses__street"><?php echo get_sub_field('street'); ?></p>
              <p class="addresses__country"><?php echo get_sub_field('town_country'); ?></p>
              <p class="addresses__number"><?php echo get_sub_field('phone_number'); ?></p>
            </div>

          <?php $delayIndex++; endwhile; ?>
      </div>
    </div>
  </div>
</section>
