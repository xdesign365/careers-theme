<section class="titled-text-block gradient-<?php echo get_field('title_gradient');?>">
  <div class="container">
    <div class="cols">
      
      <?php if(get_field('show_section_separator')): ?>
        <div class="col is-12 section-separator load-hidden">
          <div class="section-separator__line"></div>
          <span class="section-separator__text">
            <?php echo get_field('separator_text'); ?>
          </span>
        </div>
      <?php endif ?>
  
      <div class="col is-12 is-8-md">
        <h2 class="titled-text-block__title animated-gradient load-hidden">
          <?php echo get_field('title'); ?>
        </h2>
      </div>

    </div>

    <div class="cols titled-text-block__container">
      <?php while(have_rows('text_content')): the_row(); ?>
        <div class="col is-12 is-4-md titled-text-block__col no-pb load-hidden">
          <p class="titled-text-block__paragraph">
            <?php echo get_sub_field('text'); ?>
          </p>
          <?php if(get_sub_field('should_show_button')): ?>
            <a href="<?php echo get_sub_field('button_link'); ?>" class="button-link button-link--<?php echo get_sub_field('hover_color'); ?>titled-text-block__button" <?php if(get_sub_field('button_external')): ?>rel="noopener" target="_blank"<?php endif ?> >
              <span class="button-link__text">
                <?php echo get_sub_field('button_text'); ?>
              </span>
              <svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.3933 7.98292L6.42529e-07 7.98292L8.70376e-07 6.0175L13.3933 6.0175L9.29236 1.73289L11.0262 0.475028L17.2666 7.00414L11.0262 13.5254L9.29236 12.2675L13.3933 7.98292Z" fill="currentColor"/>
              </svg>
            </a>
          <?php endif ?>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
</section>
