<section class="rating-tiles gradient-<?php echo get_field('title_gradient_color'); ?>">
  <?php if(get_field('background_image')) : ?>  
    <div class="section-bg-image" style="background: linear-gradient(0deg, #090821, #090821), url(<?php echo get_field('background_image'); ?>);"></div>
  <?php endif; ?>

  <div class="container">
    <?php if(get_field('show_section_separator')): ?>
      <div class="col section-separator load-hidden">
        <div class="section-separator__line"></div>
        <span class="section-separator__text">
          <?php echo get_field('separator_text'); ?>
        </span>
      </div>
    <?php endif ?>

    <div class="cols">
      <div class="col is-12 no-pb load-hidden">
        <h2 class="rating-tiles__heading gradient-text">
          <?php echo get_field('title');?>
        </h2>
      </div>

      <?php
        while(have_rows('tiles')) : the_row();
        $isSmallRow = get_sub_field('tile_size') == 'small';
        $rating = floatval(get_sub_field('rating'));
      ?>
        <div class="col is-12 is-6-md flex <?php echo $isSmallRow? 'is-4-lg' : 'is-8-lg'; ?> load-hidden">
          <div class="rating-tiles__tile">
            <p class="rating-tiles__description">
              <?php the_sub_field('description'); ?>
            </p>

            <div class="rating-tiles__info-container">
              <strong class="rating-tiles__name">
                <?php echo get_sub_field('name_position'); ?>
              </strong>
  
              <div class="rating-tiles__rating">
                <span class="rating-tiles__rating-number"><?php echo $rating; ?></span>

                <div class="rating-tiles__stars-container">
                  <?php for($i = 0; $i < $rating; $i++) : ?>
                    <span class="rating-tiles__star" style="width: <?php echo (($rating - $i) < 1)? ($rating - floor($rating))*20 : '20'; ?>px; background-image: url('<?php echo get_sub_field('icon'); ?>')"></span>
                  <?php endfor; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; ?>

    </div>
  </div>
</section>
