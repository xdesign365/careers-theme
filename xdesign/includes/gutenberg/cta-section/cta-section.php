<section class="cta-section gradient-<?php echo get_field('gradient_color');?>">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12">
        <div class="cta-section__separator-line load-hidden"></div>
        <h2 class="super-heading cta-section__title load-hidden">
          <?php echo get_field('normal_text'); ?>
          <span class="gradient-text">
            <?php echo get_field('gradient_text'); ?>
          </span>
        </h2>
        <a href="<?php echo get_field('button_link'); ?>" class="primary-button primary-button--<?php echo get_field('button_color');?> load-hidden">
          <span class="primary-button__text">
            <?php echo get_field('button_text'); ?>
          </span>  
          <span class="primary-button__icon"></span>
        </a>
      </div>
    </div>
  </div>
</section>
