<section class="hero-b">
  <div class="section-bg-image" style="background: linear-gradient(0deg, #090821, #090821), url(<?php echo get_field('background_image'); ?>);"></div>
  <div class="container hero-b__container">
    <div class="cols">
      <h1 class="col is-12 load-hidden hero-b__heading <?php echo (str_word_count(get_field('heading')) == 2)? 'hero-b__heading--small' : ''; ?>">
        <?php echo get_field('heading'); ?>
      </h1>
    </div>
  </div>
</section>
