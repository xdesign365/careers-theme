<section class="two-columns">
  <div class="container">
    <?php while(have_rows('columns')): the_row(); ?>
      <?php $rowReversed = get_sub_field('appearance') == 'image-text'; ?>
      <div class="cols two-columns__row <?php echo ($rowReversed)? 'two-columns__row--reversed' : ''; ?>">
        <div class="col is-10 is-6-md load-hidden">
          <div class="two-columns__paragraph">
            <?php echo get_sub_field('description'); ?>
          </div>
        </div>
  
        <div class="col is-2 <?php echo ($rowReversed)? 'is-5-md' : 'is-6-md'; ?> load-hidden">
          <div class="two-columns__image">
            <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>" />
          </div>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
</section>
