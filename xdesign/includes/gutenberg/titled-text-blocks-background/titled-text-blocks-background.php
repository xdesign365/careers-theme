<section class="text-block-bg gradient-<?php echo get_field('gradient_colour'); ?> load-hidden" style="background: linear-gradient(0deg, #090821, #090821), url(<?php echo get_field('background_image'); ?>);">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12 is-6-md reveal-left">
        <h2 class="text-block-bg__title">
          <?php echo get_field('normal_text'); ?>
          <span class="text-block-bg__secondary-text gradient-text">
            <?php echo get_field('gradient_text'); ?>
          </span>
        </h2>
      </div>
      <div class="col is-12 is-4-md is-offset-2-md reveal-right">
        <p class="text-block-bg__paragraph">
          <?php echo get_field('text_content'); ?>  
        </p>
        <a href="<?php echo get_field('button_link'); ?>" class="button-link button-link--<?php echo get_field('hover_color'); ?>">
          <span class="button-link__text">
            <?php echo get_field('button_text'); ?>
          </span>
          <svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13.3933 7.98292L6.42529e-07 7.98292L8.70376e-07 6.0175L13.3933 6.0175L9.29236 1.73289L11.0262 0.475028L17.2666 7.00414L11.0262 13.5254L9.29236 12.2675L13.3933 7.98292Z" fill="currentColor"/>
          </svg>
        </a>
      </div>
    </div>
  </div>
</section>
