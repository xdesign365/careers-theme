<?php
  $delays = [125, 250, 375, 500];
?>

<section class="partners gradient-<?php echo get_field('title_gradient_color');?>">
  <div class="container">
    <div class="cols">

      <div class="col is-12">
        <h2 class="partners__heading load-hidden">
          <?php echo get_field('title');?><br>
          <span class="gradient-text">
            <?php echo get_field('gradient_title');?>
          </span>
        </h2>
      </div>

      <div class="col is-12">
        <div class="partners__mobile-logos">
          <?php $delayIndex = 0; while(have_rows('images')) : the_row(); ?>
            <?php while(have_rows('rows')) : the_row(); ?>
            <?php if(get_sub_field('image')) { ?>
              <div class="partners__image custom-load-hidden custom-load-hidden-delay-<?php echo $delays[$delayIndex]; ?>">
                <img src="<?php echo get_sub_field('image')?>" alt="<?php echo esc_attr( $attachment['alt'] ); ?>" />
              </div>
            <?php } ?>
            <?php endwhile; ?>
          <?php $delayIndex++; endwhile; ?>
        </div>
      </div>

      <div class="partners__desktop-logos">
        <?php $delayIndex = 0; while(have_rows('images')) : the_row(); ?>
          <div style="margin-left: <?php echo get_sub_field('row_offset');?>px" class="partners__logos-row load-hidden">
            <?php while(have_rows('rows')) : the_row(); ?>
            <?php if(get_sub_field('image')) { ?>
              <div class="partners__image">
                <img src="<?php echo get_sub_field('image')?>" alt="<?php echo esc_attr( $attachment['alt'] ); ?>" />
              </div>
            <?php } ?>
            <?php endwhile; ?>
          </div>
        <?php $delayIndex++; endwhile; ?>
      </div>

    </div>
  </div>
</section>
