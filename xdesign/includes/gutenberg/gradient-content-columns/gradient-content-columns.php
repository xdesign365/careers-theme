<section class="gc-columns gradient-<?php echo get_field('gradient_color');?>">
  <div class="container">
    <div class="col is-12">
      <h2>
        <?php echo get_field('title'); ?>

        <span class="highlight">
          <?php echo get_field('title_gradient_text'); ?>
        </span>
      </h2>
    </div>
  </div>

  <div class="container">
    <?php $columns = get_field('columns_per_row'); while(have_rows('columns')) : the_row(); ?>
      <div class="col is-<?php echo $columns; ?>">
        <div class="gc-columns__column gradient-text">
          <?php echo get_sub_field('content'); ?>
        </div>
      </div>      
    <?php endwhile; ?>
  </div>
</section>
