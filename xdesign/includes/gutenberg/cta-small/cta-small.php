<section class="cta-small gradient-<?php echo get_field('gradient_color'); ?> <?php echo get_field('extra_css_class'); ?>">
  <div class="container">

    <div class="cols">
      <div class="col is-12 is-8-md load-hidden">
        <h2 class="cta-small__heading">
          <?php echo get_field('title'); ?>
          <span class="gradient-text">
            <?php echo get_field('gradient_text'); ?>
          </span>
        </h2>
      </div>
    </div>

    <div class="cols">
      <div class="col is-12 is-8-md cta-small__paragraph highlight-<?php echo get_field('button_color');?> load-hidden">
        <?php echo get_field('paragraph'); ?>
      </div>

      <div class="col is-12 is-4-md is-offset-1-lg is-3-lg load-hidden">
        <a href="<?php echo get_field('button_link'); ?>" class="primary-button primary-button--<?php echo get_field('button_color');?>">
          <span class="primary-button__text">
            <?php echo get_field('button_text'); ?>
          </span>  
          <span class="primary-button__icon"></span>
        </a>
      </div>
    </div>

  </div>
</section>
