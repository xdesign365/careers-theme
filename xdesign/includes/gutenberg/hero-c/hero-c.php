<section class="hero-c">
  <div class="section-bg-image" style="background: linear-gradient(0deg, #090821, #090821), url(<?php echo get_field('background_image'); ?>);"></div>
  <div class="container hero-c__container">
    <h1 class="hero-c__heading load-hidden">
      <?php while(have_rows('headings')): the_row(); ?>
        <span>
          <?php echo get_sub_field('heading'); ?>
        </span>
        <br>
      <?php endwhile; ?>
    </h1>
    <div class="col is-12 is-6-sm is-offset-6-sm hero-c__numbers-container symbol-info-numbers no-pb load-hidden">
        <?php while(have_rows('numbers')): the_row(); ?>
          <div class="col no-pb countupjs">
            <h2 class="symbol-info-numbers__number value" data-value="<?php echo get_sub_field('number'); ?>">
              <?php echo get_sub_field('number'); ?>
            </h2>
            <span class="symbol-info-numbers__text">
              <?php echo get_sub_field('text'); ?>
            </span>
          </div>
        <?php endwhile; ?>
      </div>  
  </div>
</section>
