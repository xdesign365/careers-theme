<?php
  $delayIndex = 0;
  $delays = [250, 500, 750, 1000];

  $should_show_stats = get_field('should_show_numbers');
  $statsRow = [];

  if ($should_show_stats) {
    if (class_exists('GreenhouseApi')) {
      $greenhouse_api = new GreenhouseApi;
      $greenhouse_api_filters = $greenhouse_api->generate_tiered_departments();
      $greenhouse_api_locations = $greenhouse_api->filter_real_locations();
      $greenhouse_api_jobs = $greenhouse_api->greenhouse_feed_jobs();
      array_push($statsRow, ['label' => '', 'total' => count($greenhouse_api_jobs)]);
      array_push($statsRow, ['label' => '', 'total' => count($greenhouse_api_filters)]);
      array_push($statsRow, ['label' => '', 'total' => count($greenhouse_api_locations)]);

      $number_placement_iterator = 0;
    
      while(have_rows('numbers_content')) {
        the_row();
        $columnValue = get_sub_field('number');
        $columnLabel = get_sub_field('text');
      $statsRow[$number_placement_iterator]['label'] = $columnLabel;
        if (isset($columnValue) && $columnValue > 0) {
          $statsRow[$number_placement_iterator]['total'] = $columnValue;
        }
        $number_placement_iterator++;
      }
    } 
  }
?>

<section class="hero-a no-margin">
  <div class="section-bg-image" style="background: linear-gradient(0deg, #090821, #090821), url(<?php echo get_field('background_image'); ?>);"></div>
  <div class="container hero-a__container">
    <div class="cols">
      <h1 class="col hero-a__title">
        <?php while(have_rows('titles')): the_row(); ?>
          <span class="gradient-<?php echo get_sub_field('gradient_color'); ?> gradient-text custom-load-hidden custom-load-hidden-delay-<?php echo $delays[$delayIndex]; ?>">
            <?php echo get_sub_field('title'); ?>
          </span>
        <?php $delayIndex++; endwhile; ?>
      </h1>
    </div>

    <?php if(get_field('show_button')): ?>
      <div>
        <a href="<?php echo get_field('button_url'); ?>" class="hero-a__button primary-button primary-button--<?php echo get_field('button_colour'); ?> load-hidden">
          <span class="primary-button__text">
            <?php echo get_field('button_text'); ?>
          </span>
          <span class="primary-button__icon"></span>
        </a>
      </div>
    <?php endif ?>

    <?php if($should_show_stats): ?>
      <div class="cols">
        <div class="col is-12 is-6-md is-offset-6-md hero-a__numbers-container symbol-info-numbers load-hidden">
          <?php 
            foreach ($statsRow as $row) {
              if ($row['total'] > 0) {
                echo '<div class="hero-a__number countupjs">';
                echo '<h2 class="symbol-info-numbers__number value" data-value="' . $row['total'] . '"></h2>';
                echo '<span class="symbol-info-numbers__text">' . $row['label'] . '</span>';
                echo '</div>';
              }
            }
          ?>
        </div>
      </div>
    <?php endif ?>  
  </div>
</section>
