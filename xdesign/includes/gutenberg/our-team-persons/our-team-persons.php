<section class="our-story-persons">
    <div class="container">
        <div class="cols">

        <?php 
            $divider_text = get_field('divider_text'); 

            if($divider_text) { ?>

            <div class="col is-12 section-separator load-hidden">
                <div class="section-separator__line"></div>
                <span class="section-separator__text load-hidden">
                    <?php echo $divider_text; ?>
                </span>
            </div>

            <?php }; 
        ?>

        <?php
            if( have_rows('persons_collection') ) :
                while( have_rows('persons_collection') ) : the_row();

                // Loading sub values.
                $image = get_sub_field('image');
                $name = get_sub_field('name');
                $role = get_sub_field('role');

                ?>

                    <div class="col is-12 is-6-sm is-4-md our-story-persons__person-tab">
                        <div class="our-story-persons__image-wrapper">
                            <?php
                                echo wp_get_attachment_image($image, 'full' );
                            ?>
                        </div>
                        <h3 class="our-story-persons__person-name"><?php echo $name; ?></h3>
                        <p class="our-story-persons__person-role"><?php echo $role; ?></p>

                    </div>

                <?php

                endwhile;
            endif;
        ?>

        </div>
    </div>

</section>
