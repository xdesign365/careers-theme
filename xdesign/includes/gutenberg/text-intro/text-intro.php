<section class="text-intro">
  <div class="container">
    <div class="cols">
      <div class="col is-12 is-8-md load-hidden">
        <p class="sub-heading">
          <?php echo get_field('text'); ?>
        </p>
      </div>
    </div>
  </div>
</section>
