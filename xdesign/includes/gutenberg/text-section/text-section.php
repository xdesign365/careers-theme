<section class="text-section">
  <div class="container">
    <div class="cols">
      <div class="col is-12 no-pb load-hidden">
        <div class="text-section__content">
          <?php echo get_field('content'); ?>
        </div>
      </div>
    </div>
  </div>
</section>
