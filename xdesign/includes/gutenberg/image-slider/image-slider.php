<section class="image-slider load-hidden">
  <div class="image-slider__row">
    <?php while(have_rows('images_row')): the_row(); ?>
      <div class="image-slider__col">
        <?php $numberOfImages=count(get_sub_field('image_column')); ?>
        <?php while(have_rows('image_column')): the_row(); ?>
          <div class="image-slider__image-container <?php echo ($numberOfImages>=2) ? 'image-slider__image-container--two-images' : '';?>">
            <img class="image-slider__image" src="<?php echo get_sub_field('image'); ?>" alt="" srcset="">
            <div class="image-slider__gradient-image" style="background: linear-gradient(0deg, #090821, #090821), url(<?php echo get_sub_field('image'); ?>);"></div>
          </div>
        <?php endwhile; ?>
      </div>
    <?php endwhile; ?>
  </div>
</section>
