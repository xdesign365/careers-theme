<section class="it-tiles gradient-<?php echo get_field('title_gradient_color'); ?>">
  <div class="container">
    <div class="cols">
      <?php if(get_field('show_section_separator')): ?>
        <div class="col is-12 section-separator load-hidden">
          <div class="section-separator__line"></div>
          <span class="section-separator__text">
            <?php echo get_field('separator_text'); ?>
          </span>
        </div>
      <?php endif ?>
        <div class="col is-12 is-4-md load-hidden">
          <h2 class="it-tiles__title animated-gradient">
            <?php echo get_field('title');?>
          </h2>
        </div>
    </div>

    <div class="cols">
      <?php while(have_rows('tiles')) : the_row(); ?>
        <div class="col is-12 is-6-md no-pb load-hidden">
          <div class="it-tiles__tile">

            <div class="it-tiles__tile-header">
              <img class="it-tiles__icon" src="<?php echo get_sub_field('icon');?>" alt="" />
              <?php if(get_sub_field('title')): ?>
              <h3 class="it-tiles__tile-title">
                <?php echo get_sub_field('title'); ?>
              </h3>
              <?php endif ?>
            </div>
  
            <p class="it-tiles__tile-description">
              <?php echo get_sub_field('description'); ?>
            </p>
          </div>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
</section>
