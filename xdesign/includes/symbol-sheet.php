
<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12">
        <h3 class="no-margin">Primary buttons  <br /><code>.symbol-primary-button</code></h3>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--mint-green">
          View our open roles
          <span class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--mint-green hover">
          View our open roles
          <span class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Focus</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--mint-green focus">
          View our open roles
          <span class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Disabled</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--mint-green disabled">
          View our open roles
          <span disabled class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--heliotrope-purple">
          View our open roles
          <span class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-primary-button symbol-primary-butto--heliotrope-purple hover">
          View our open roles
          <span class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Focus</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--heliotrope-purple focus">
          View our open roles
          <span class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Disabled</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--heliotrope-purple disabled">
          View our open roles
          <span disabled class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--cerulean-blue">
          View our open roles
          <span class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--cerulean-blue hover">
          View our open roles
          <span class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Focus</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--cerulean-blue focus">
          View our open roles
          <span class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Disabled</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--cerulean-blue disabled">
          View our open roles
          <span disabled class="symbol-primary-button__icon"></span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--mint-green symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--mint-green hover symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Focus</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--mint-green focus symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Disabled</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--mint-green disabled symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--heliotrope-purple symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-primary-button symbol-primary-butto--heliotrope-purple hover symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Focus</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--heliotrope-purple focus symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Disabled</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--heliotrope-purple disabled symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--cerulean-blue symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--cerulean-blue hover symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Focus</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--cerulean-blue focus symbol-primary-button--small">Show more</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Disabled</h6>
        <a href="/" class="symbol-primary-button symbol-primary-button--cerulean-blue disabled symbol-primary-button--small">Show more</a>
      </div>
    </div>
  </div>
</section>


<section class="symbol-section symbol-section--small">
  <div class="container overflow-visible">
    <div class="separator"></div>
  </div>
</section>

<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12">
        <h3 class="no-margin">Link buttons  <br /><code>.symbol-link-button</code></h3>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-link-button symbol-link-button--mint-green">
          Get in touch
          <span class="symbol-link-button__icon">
            <svg width="18" height="14" viewBox="0 0 18 14" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.3933 7.98292L6.42529e-07 7.98292L8.70376e-07 6.0175L13.3933 6.0175L9.29236 1.73289L11.0262 0.475028L17.2666 7.00414L11.0262 13.5254L9.29236 12.2675L13.3933 7.98292Z" fill="currentColor"/>
            </svg>
          </span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-link-button hover symbol-link-button--mint-green">
          Get in touch
          <span class="symbol-link-button__icon">
            <svg width="18" height="14" viewBox="0 0 18 14" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.3933 7.98292L6.42529e-07 7.98292L8.70376e-07 6.0175L13.3933 6.0175L9.29236 1.73289L11.0262 0.475028L17.2666 7.00414L11.0262 13.5254L9.29236 12.2675L13.3933 7.98292Z" fill="currentColor"/>
            </svg>
          </span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Focus</h6>
        <a href="/" class="symbol-link-button focus symbol-link-button--mint-green">
          Get in touch
          <span class="symbol-link-button__icon">
            <svg width="18" height="14" viewBox="0 0 18 14" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.3933 7.98292L6.42529e-07 7.98292L8.70376e-07 6.0175L13.3933 6.0175L9.29236 1.73289L11.0262 0.475028L17.2666 7.00414L11.0262 13.5254L9.29236 12.2675L13.3933 7.98292Z" fill="currentColor"/>
            </svg>
          </span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Disabled</h6>
        <a href="/" class="symbol-link-button disabled symbol-link-button--mint-green">
          Get in touch
          <span class="symbol-link-button__icon">
            <svg width="18" height="14" viewBox="0 0 18 14" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.3933 7.98292L6.42529e-07 7.98292L8.70376e-07 6.0175L13.3933 6.0175L9.29236 1.73289L11.0262 0.475028L17.2666 7.00414L11.0262 13.5254L9.29236 12.2675L13.3933 7.98292Z" fill="currentColor"/>
            </svg>
          </span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-link-button symbol-link-button--heliotrope-purple">
          Get in touch
          <span class="symbol-link-button__icon">
            <svg width="18" height="14" viewBox="0 0 18 14" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.3933 7.98292L6.42529e-07 7.98292L8.70376e-07 6.0175L13.3933 6.0175L9.29236 1.73289L11.0262 0.475028L17.2666 7.00414L11.0262 13.5254L9.29236 12.2675L13.3933 7.98292Z" fill="currentColor"/>
            </svg>
          </span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-link-button hover symbol-link-button--heliotrope-purple">
          Get in touch
          <span class="symbol-link-button__icon">
            <svg width="18" height="14" viewBox="0 0 18 14" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.3933 7.98292L6.42529e-07 7.98292L8.70376e-07 6.0175L13.3933 6.0175L9.29236 1.73289L11.0262 0.475028L17.2666 7.00414L11.0262 13.5254L9.29236 12.2675L13.3933 7.98292Z" fill="currentColor"/>
            </svg>
          </span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Focus</h6>
        <a href="/" class="symbol-link-button focus symbol-link-button--heliotrope-purple">
          Get in touch
          <span class="symbol-link-button__icon">
            <svg width="18" height="14" viewBox="0 0 18 14" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.3933 7.98292L6.42529e-07 7.98292L8.70376e-07 6.0175L13.3933 6.0175L9.29236 1.73289L11.0262 0.475028L17.2666 7.00414L11.0262 13.5254L9.29236 12.2675L13.3933 7.98292Z" fill="currentColor"/>
            </svg>
          </span>
        </a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Disabled</h6>
        <a href="/" class="symbol-link-button disabled symbol-link-button--heliotrope-purple">
          Get in touch
          <span class="symbol-link-button__icon">
            <svg width="18" height="14" viewBox="0 0 18 14" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.3933 7.98292L6.42529e-07 7.98292L8.70376e-07 6.0175L13.3933 6.0175L9.29236 1.73289L11.0262 0.475028L17.2666 7.00414L11.0262 13.5254L9.29236 12.2675L13.3933 7.98292Z" fill="currentColor"/>
            </svg>
          </span>
        </a>
      </div>
    </div>
  </div>
</section>

<section class="symbol-section symbol-section--small">
  <div class="container overflow-visible">
    <div class="separator"></div>
  </div>
</section>

<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12">
        <h3 class="no-margin">Email buttons  <br /><code>.symbol-email-button</code></h3>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-email-button symbol-email-button--mint-green">support@xdesign.com</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-email-button hover symbol-email-button--mint-green">support@xdesign.com</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-email-button symbol-email-button--heliotrope-purple">support@xdesign.com</a>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-email-button hover symbol-email-button--heliotrope-purple">support@xdesign.com</a>
      </div>
    </div>
  </div>
</section>

<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols flex-column">
      <div class="col is-12">
        <h3 class="no-margin">Footer buttons  <br /><code>.symbol-footer-button</code></h3>
      </div>
      <div class="col is-12 is-4-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-footer-button">
          <span class="symbol-footer-button__text">Careers Home</span>
          <svg class="symbol-footer-button__icon" width="20" height="18" viewBox="0 0 20 18" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.693 10.3328L0.800052 10.3328L0.800052 7.66766L15.693 7.66766L11.1329 1.85757L13.0609 0.151857L20 9.00558L13.0609 17.8486L11.1329 16.1429L15.693 10.3328Z" fill="currentColor"/>
          </svg>
        </a>
      </div>
      <div class="col is-12 is-4-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-footer-button hover">
          <span class="symbol-footer-button__text">Careers Home</span>
          <svg class="symbol-footer-button__icon" width="20" height="18" viewBox="0 0 20 18" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.693 10.3328L0.800052 10.3328L0.800052 7.66766L15.693 7.66766L11.1329 1.85757L13.0609 0.151857L20 9.00558L13.0609 17.8486L11.1329 16.1429L15.693 10.3328Z" fill="currentColor"/>
          </svg>
        </a>
      </div>
      <div class="col is-12 is-4-md">
        <h6 class="margins">Focus</h6>
        <a href="/" class="symbol-footer-button focus">
          <span class="symbol-footer-button__text">Careers Home</span>
          <svg class="symbol-footer-button__icon" width="20" height="18" viewBox="0 0 20 18" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.693 10.3328L0.800052 10.3328L0.800052 7.66766L15.693 7.66766L11.1329 1.85757L13.0609 0.151857L20 9.00558L13.0609 17.8486L11.1329 16.1429L15.693 10.3328Z" fill="currentColor"/>
          </svg>
        </a>
      </div>
      <div class="col is-12 is-4-md">
        <h6 class="margins">Focus + Gradient Text</h6>
        <a href="/" class="symbol-footer-button symbol-footer-button--gradient-text focus">
          <span class="symbol-footer-button__text">Careers Home</span>
          <svg class="symbol-footer-button__icon" width="20" height="18" viewBox="0 0 20 18" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.693 10.3328L0.800052 10.3328L0.800052 7.66766L15.693 7.66766L11.1329 1.85757L13.0609 0.151857L20 9.00558L13.0609 17.8486L11.1329 16.1429L15.693 10.3328Z" fill="currentColor"/>
          </svg>
        </a>
      </div>
    </div>
  </div>
</section>

<section class="symbol-section symbol-section--small">
  <div class="container overflow-visible">
    <div class="separator"></div>
  </div>
</section>

<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols flex-column">
      <div class="col is-12">
        <h3 class="no-margin">Team buttons  <br /><code>.symbol-team-button</code></h3>
      </div>
      <div class="col is-12 is-4-md">
        <h6 class="margins">Default</h6>
        <a href="/" class="symbol-team-button">
          <div class="symbol-team-button__header">
            <div class="symbol-team-button__icon symbol-team-button__icon--left">
              <svg width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M19.8126 15.788L21.1648 16.5992C21.2482 16.6491 21.3173 16.7198 21.3652 16.8044C21.4132 16.889 21.4384 16.9846 21.4384 17.0818C21.4384 17.179 21.4132 17.2746 21.3652 17.3592C21.3173 17.4438 21.2482 17.5145 21.1648 17.5644L11.2986 23.4842C11.1236 23.5893 10.9233 23.6448 10.7192 23.6448C10.5151 23.6448 10.3148 23.5893 10.1398 23.4842L0.273572 17.5644C0.190149 17.5145 0.121096 17.4438 0.0731492 17.3592C0.0252026 17.2746 0 17.179 0 17.0818C0 16.9846 0.0252026 16.889 0.0731492 16.8044C0.121096 16.7198 0.190149 16.6491 0.273572 16.5992L1.62582 15.788L10.7192 21.2443L19.8126 15.788ZM19.8126 10.5005L21.1648 11.3117C21.2482 11.3616 21.3173 11.4323 21.3652 11.5169C21.4132 11.6015 21.4384 11.6971 21.4384 11.7943C21.4384 11.8915 21.4132 11.9871 21.3652 12.0717C21.3173 12.1563 21.2482 12.227 21.1648 12.2769L10.7192 18.5443L0.273572 12.2769C0.190149 12.227 0.121096 12.1563 0.0731492 12.0717C0.0252026 11.9871 0 11.8915 0 11.7943C0 11.6971 0.0252026 11.6015 0.0731492 11.5169C0.121096 11.4323 0.190149 11.3616 0.273572 11.3117L1.62582 10.5005L10.7192 15.9568L19.8126 10.5005ZM11.2974 0.160661L21.1648 6.08041C21.2482 6.13035 21.3173 6.20107 21.3652 6.28565C21.4132 6.37024 21.4384 6.46581 21.4384 6.56304C21.4384 6.66026 21.4132 6.75583 21.3652 6.84042C21.3173 6.92501 21.2482 6.99572 21.1648 7.04566L10.7192 13.313L0.273572 7.04566C0.190149 6.99572 0.121096 6.92501 0.0731492 6.84042C0.0252026 6.75583 0 6.66026 0 6.56304C0 6.46581 0.0252026 6.37024 0.0731492 6.28565C0.121096 6.20107 0.190149 6.13035 0.273572 6.08041L10.1398 0.160661C10.3148 0.0555372 10.5151 0 10.7192 0C10.9233 0 11.1236 0.0555372 11.2986 0.160661H11.2974ZM10.7192 2.43654L3.84207 6.56304L10.7192 10.6895L17.5963 6.56304L10.7192 2.43654Z" fill="#FAFAFB"/>
              </svg>
            </div>
            <span class="symbol-team-button__name">Team Name</span>
            <div class="symbol-team-button__icon symbol-team-button__icon--right">
              <svg width="20" height="18" viewBox="0 0 20 18" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M15.693 10.3328L0.800052 10.3328L0.800052 7.66766L15.693 7.66766L11.1329 1.85757L13.0609 0.151857L20 9.00558L13.0609 17.8486L11.1329 16.1429L15.693 10.3328Z" fill="currentColor"/>
              </svg>
            </div>
          </div>
          <p class="symbol-team-button__paragraph">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a consectetur ante, vitae volutpat augue.
          </p>
        </a>
      </div>
      <div class="col is-12 is-4-md">
        <h6 class="margins">Hover</h6>
        <a href="/" class="symbol-team-button hover">
          <div class="symbol-team-button__header">
            <div class="symbol-team-button__icon symbol-team-button__icon--left">
              <svg width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M19.8126 15.788L21.1648 16.5992C21.2482 16.6491 21.3173 16.7198 21.3652 16.8044C21.4132 16.889 21.4384 16.9846 21.4384 17.0818C21.4384 17.179 21.4132 17.2746 21.3652 17.3592C21.3173 17.4438 21.2482 17.5145 21.1648 17.5644L11.2986 23.4842C11.1236 23.5893 10.9233 23.6448 10.7192 23.6448C10.5151 23.6448 10.3148 23.5893 10.1398 23.4842L0.273572 17.5644C0.190149 17.5145 0.121096 17.4438 0.0731492 17.3592C0.0252026 17.2746 0 17.179 0 17.0818C0 16.9846 0.0252026 16.889 0.0731492 16.8044C0.121096 16.7198 0.190149 16.6491 0.273572 16.5992L1.62582 15.788L10.7192 21.2443L19.8126 15.788ZM19.8126 10.5005L21.1648 11.3117C21.2482 11.3616 21.3173 11.4323 21.3652 11.5169C21.4132 11.6015 21.4384 11.6971 21.4384 11.7943C21.4384 11.8915 21.4132 11.9871 21.3652 12.0717C21.3173 12.1563 21.2482 12.227 21.1648 12.2769L10.7192 18.5443L0.273572 12.2769C0.190149 12.227 0.121096 12.1563 0.0731492 12.0717C0.0252026 11.9871 0 11.8915 0 11.7943C0 11.6971 0.0252026 11.6015 0.0731492 11.5169C0.121096 11.4323 0.190149 11.3616 0.273572 11.3117L1.62582 10.5005L10.7192 15.9568L19.8126 10.5005ZM11.2974 0.160661L21.1648 6.08041C21.2482 6.13035 21.3173 6.20107 21.3652 6.28565C21.4132 6.37024 21.4384 6.46581 21.4384 6.56304C21.4384 6.66026 21.4132 6.75583 21.3652 6.84042C21.3173 6.92501 21.2482 6.99572 21.1648 7.04566L10.7192 13.313L0.273572 7.04566C0.190149 6.99572 0.121096 6.92501 0.0731492 6.84042C0.0252026 6.75583 0 6.66026 0 6.56304C0 6.46581 0.0252026 6.37024 0.0731492 6.28565C0.121096 6.20107 0.190149 6.13035 0.273572 6.08041L10.1398 0.160661C10.3148 0.0555372 10.5151 0 10.7192 0C10.9233 0 11.1236 0.0555372 11.2986 0.160661H11.2974ZM10.7192 2.43654L3.84207 6.56304L10.7192 10.6895L17.5963 6.56304L10.7192 2.43654Z" fill="#FAFAFB"/>
              </svg>
            </div>
            <span class="symbol-team-button__name">Team Name</span>
            <div class="symbol-team-button__icon symbol-team-button__icon--right">
              <svg width="20" height="18" viewBox="0 0 20 18" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M15.693 10.3328L0.800052 10.3328L0.800052 7.66766L15.693 7.66766L11.1329 1.85757L13.0609 0.151857L20 9.00558L13.0609 17.8486L11.1329 16.1429L15.693 10.3328Z" fill="currentColor"/>
              </svg>
            </div>
          </div>
          <p class="symbol-team-button__paragraph">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a consectetur ante, vitae volutpat augue.
          </p>
        </a>
      </div>
      <div class="col is-12 is-4-md">
        <h6 class="margins">Focus</h6>
        <a href="/" class="symbol-team-button focus">
          <div class="symbol-team-button__header">
            <div class="symbol-team-button__icon symbol-team-button__icon--left">
              <svg width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M19.8126 15.788L21.1648 16.5992C21.2482 16.6491 21.3173 16.7198 21.3652 16.8044C21.4132 16.889 21.4384 16.9846 21.4384 17.0818C21.4384 17.179 21.4132 17.2746 21.3652 17.3592C21.3173 17.4438 21.2482 17.5145 21.1648 17.5644L11.2986 23.4842C11.1236 23.5893 10.9233 23.6448 10.7192 23.6448C10.5151 23.6448 10.3148 23.5893 10.1398 23.4842L0.273572 17.5644C0.190149 17.5145 0.121096 17.4438 0.0731492 17.3592C0.0252026 17.2746 0 17.179 0 17.0818C0 16.9846 0.0252026 16.889 0.0731492 16.8044C0.121096 16.7198 0.190149 16.6491 0.273572 16.5992L1.62582 15.788L10.7192 21.2443L19.8126 15.788ZM19.8126 10.5005L21.1648 11.3117C21.2482 11.3616 21.3173 11.4323 21.3652 11.5169C21.4132 11.6015 21.4384 11.6971 21.4384 11.7943C21.4384 11.8915 21.4132 11.9871 21.3652 12.0717C21.3173 12.1563 21.2482 12.227 21.1648 12.2769L10.7192 18.5443L0.273572 12.2769C0.190149 12.227 0.121096 12.1563 0.0731492 12.0717C0.0252026 11.9871 0 11.8915 0 11.7943C0 11.6971 0.0252026 11.6015 0.0731492 11.5169C0.121096 11.4323 0.190149 11.3616 0.273572 11.3117L1.62582 10.5005L10.7192 15.9568L19.8126 10.5005ZM11.2974 0.160661L21.1648 6.08041C21.2482 6.13035 21.3173 6.20107 21.3652 6.28565C21.4132 6.37024 21.4384 6.46581 21.4384 6.56304C21.4384 6.66026 21.4132 6.75583 21.3652 6.84042C21.3173 6.92501 21.2482 6.99572 21.1648 7.04566L10.7192 13.313L0.273572 7.04566C0.190149 6.99572 0.121096 6.92501 0.0731492 6.84042C0.0252026 6.75583 0 6.66026 0 6.56304C0 6.46581 0.0252026 6.37024 0.0731492 6.28565C0.121096 6.20107 0.190149 6.13035 0.273572 6.08041L10.1398 0.160661C10.3148 0.0555372 10.5151 0 10.7192 0C10.9233 0 11.1236 0.0555372 11.2986 0.160661H11.2974ZM10.7192 2.43654L3.84207 6.56304L10.7192 10.6895L17.5963 6.56304L10.7192 2.43654Z" fill="#FAFAFB"/>
              </svg>
            </div>
            <span class="symbol-team-button__name">Team Name</span>
            <div class="symbol-team-button__icon symbol-team-button__icon--right">
              <svg width="20" height="18" viewBox="0 0 20 18" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M15.693 10.3328L0.800052 10.3328L0.800052 7.66766L15.693 7.66766L11.1329 1.85757L13.0609 0.151857L20 9.00558L13.0609 17.8486L11.1329 16.1429L15.693 10.3328Z" fill="currentColor"/>
              </svg>
            </div>
          </div>
          <p class="symbol-team-button__paragraph">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a consectetur ante, vitae volutpat augue.
          </p>
        </a>
      </div>
    </div>
  </div>
</section>

<section class="symbol-section symbol-section--small">
  <div class="container overflow-visible">
    <div class="separator"></div>
  </div>
</section>

<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12">
        <h3 class="no-margin">Checkboxes  <br /><code>.symbol-checkbox</code></h3>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Default</h6>
        <label class="symbol-checkbox">
            <input type="checkbox" class="symbol-checkbox__input">
            <span class="symbol-checkbox__checkmark"></span>
        </label>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Default + Hover</h6>
        <label class="symbol-checkbox hover">
            <input type="checkbox" class="symbol-checkbox__input">
            <span class="symbol-checkbox__checkmark"></span>
        </label>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Selected</h6>
        <label class="symbol-checkbox">
            <input type="checkbox" class="symbol-checkbox__input checked">
            <span class="symbol-checkbox__checkmark"></span>
        </label>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Selected + Hover</h6>
        <label class="symbol-checkbox hover">
            <input type="checkbox" class="symbol-checkbox__input checked">
            <span class="symbol-checkbox__checkmark"></span>
        </label>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Disabled</h6>
        <label class="symbol-checkbox">
            <input type="checkbox" class="symbol-checkbox__input disabled">
            <span class="symbol-checkbox__checkmark"></span>
        </label>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Selected + Disabled</h6>
        <label class="symbol-checkbox">
            <input type="checkbox" class="symbol-checkbox__input checked disabled">
            <span class="symbol-checkbox__checkmark"></span>
        </label>
      </div>
    </div>
  </div>
</section>

<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12">
        <h3 class="no-margin">Radio  <br /><code>.symbol-radio</code></h3>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Default</h6>
        <label class="symbol-radio">
            <input type="radio" class="symbol-radio__input">
            <span class="symbol-radio__circle"></span>
        </label>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Default + Hover</h6>
        <label class="symbol-radio hover">
            <input type="radio" class="symbol-radio__input">
            <span class="symbol-radio__circle"></span>
        </label>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Selected</h6>
        <label class="symbol-radio">
            <input type="radio" class="symbol-radio__input checked">
            <span class="symbol-radio__circle"></span>
        </label>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Selected + Hover</h6>
        <label class="symbol-radio hover">
            <input type="radio" class="symbol-radio__input checked">
            <span class="symbol-radio__circle"></span>
        </label>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Disabled</h6>
        <label class="symbol-radio">
            <input type="radio" class="symbol-radio__input disabled">
            <span class="symbol-radio__circle"></span>
        </label>
      </div>
      <div class="col is-12 is-2-md">
        <h6 class="margins">Selected + Disabled</h6>
        <label class="symbol-radio">
            <input type="radio" class="symbol-radio__input checked disabled">
            <span class="symbol-radio__circle"></span>
        </label>
      </div>
    </div>
  </div>
</section>

<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12">
        <h3 class="no-margin">Toggles  <br /><code>.symbol-toggle</code></h3>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Off</h6>
        <label class="symbol-toggle">
            <input type="checkbox" class="symbol-toggle__input">
            <span class="symbol-toggle__slider"></span>
        </label>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">Off + Hover</h6>
        <label class="symbol-toggle hover">
            <input type="checkbox" class="symbol-toggle__input">
            <span class="symbol-toggle__slider"></span>
        </label>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">On</h6>
        <label class="symbol-toggle">
            <input type="checkbox" class="symbol-toggle__input checked">
            <span class="symbol-toggle__slider"></span>
        </label>
      </div>
      <div class="col is-12 is-3-md">
        <h6 class="margins">On + Hover</h6>
        <label class="symbol-toggle hover">
            <input type="checkbox" class="symbol-toggle__input checked">
            <span class="symbol-toggle__slider"></span>
        </label>
      </div>
    </div>
  </div>
</section>

<section class="symbol-section symbol-section--small">
  <div class="container overflow-visible">
    <div class="separator"></div>
  </div>
</section>

<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12">
        <h3 class="no-margin">Forms  <br /><code>.symbol-form__input</code> && <code>.symbol-form__label</code></h3>
      </div>
      <div class="col is-12 is-6-md">
        <div class="symbol-form">
          <h6 class="margins">Default</h6>
          <label class="symbol-form__label">Label</label>
          <input type="email" class="symbol-form__input" placeholder="jim.person@email.com">
        </div>
        <div class="symbol-form">
          <h6 class="margins">Hover</h6>
          <label class="symbol-form__label">Label</label>
          <input type="email" class="symbol-form__input hover" placeholder="jim.person@email.com">
        </div>
        <div class="symbol-form">
          <h6 class="margins">Focus</h6>
          <label class="symbol-form__label">Label</label>
          <input type="email" class="symbol-form__input focus" placeholder="jim.person@email.com">
        </div>
        <div class="symbol-form">
          <h6 class="margins">Filled</h6>
          <label class="symbol-form__label">Label</label>
          <input type="email" class="symbol-form__input" value="jim.person@email.com" placeholder="jim.person@email.com">
        </div>
        <div class="symbol-form">
          <h6 class="margins">Error</h6>
          <label class="symbol-form__label">Label</label>
          <input type="email" class="symbol-form__input symbol-form__input--error" value="jim.person@emailcom" placeholder="jim.person@email.com">
          <span class="symbol-form__error">Please enter a valid email address</span>
        </div>
        <div class="symbol-form">
          <h6 class="margins">Default</h6>
          <label class="symbol-form__label">Label</label>
          <textarea class="symbol-form__input symbol-form__input--textarea" placeholder="Type in your message"></textarea>
        </div>
        <div class="symbol-form">
          <h6 class="margins">Hover</h6>
          <label class="symbol-form__label">Label</label>
          <textarea class="symbol-form__input hover" placeholder="Type in your message"></textarea>
        </div>
        <div class="symbol-form">
          <h6 class="margins">Typing</h6>
          <label class="symbol-form__label">Label</label>
          <textarea class="symbol-form__input focus" placeholder="Type in your message">Typing your message|</textarea>
        </div>
        <div class="symbol-form">
          <h6 class="margins">Filled</h6>
          <label class="symbol-form__label">Label</label>
          <textarea class="symbol-form__input symbol-form__input--textarea" value="jim.person@email.com" placeholder="Type in your message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a consectetur ante, vitae volutpat augue. Integer mattis diam quam, suscipit tincidunt leo porta fermentum. Phasellus non sapien mi. Suspendisse mattis pretium metu.</textarea>
        </div>
        <div class="symbol-form">
          <h6 class="margins">Description</h6>
          <label class="symbol-form__label">Label</label>
          <textarea class="symbol-form__input symbol-form__input--textarea" value="jim.person@emailcom" placeholder="Type in your message"></textarea>
          <span class="symbol-form__description">
            Mauris tincidunt ultricies est vitae placerat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac est vitae placerat <strong>Privacy Policy.</strong>
          </span>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="symbol-section symbol-section--small">
  <div class="container overflow-visible">
    <div class="separator"></div>
  </div>
</section>

<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12">
        <h3 class="no-margin">Search bar  <br /><code>.symbol-searchbar</code></h3>
      </div>
      <div class="col is-12 is-6-md">
        <h6 class="margins">Default</h6>
        <div class="symbol-searchbar">
          <div class="symbol-searchbar__icon symbol-searchbar__icon--left">
            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M14.0259 12.8478L17.5951 16.4162L16.4159 17.5953L12.8476 14.0262C11.5199 15.0905 9.86842 15.6694 8.16675 15.667C4.02675 15.667 0.666748 12.307 0.666748 8.16699C0.666748 4.02699 4.02675 0.666992 8.16675 0.666992C12.3067 0.666992 15.6667 4.02699 15.6667 8.16699C15.6692 9.86866 15.0903 11.5201 14.0259 12.8478ZM12.3542 12.2295C13.4118 11.1419 14.0025 9.68402 14.0001 8.16699C14.0001 4.94366 11.3892 2.33366 8.16675 2.33366C4.94341 2.33366 2.33341 4.94366 2.33341 8.16699C2.33341 11.3895 4.94341 14.0003 8.16675 14.0003C9.68377 14.0027 11.1417 13.4121 12.2292 12.3545L12.3542 12.2295Z" fill="currentColor"/>
            </svg>
          </div>
          <input type="search" class="symbol-searchbar__input" placeholder="Search jobs...">
          <div class="symbol-searchbar__icon symbol-searchbar__icon--right"></div>
        </div>
        <h6 class="margins">Hover</h6>
        <div class="symbol-searchbar hover">
          <div class="symbol-searchbar__icon symbol-searchbar__icon--left">
            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M14.0259 12.8478L17.5951 16.4162L16.4159 17.5953L12.8476 14.0262C11.5199 15.0905 9.86842 15.6694 8.16675 15.667C4.02675 15.667 0.666748 12.307 0.666748 8.16699C0.666748 4.02699 4.02675 0.666992 8.16675 0.666992C12.3067 0.666992 15.6667 4.02699 15.6667 8.16699C15.6692 9.86866 15.0903 11.5201 14.0259 12.8478ZM12.3542 12.2295C13.4118 11.1419 14.0025 9.68402 14.0001 8.16699C14.0001 4.94366 11.3892 2.33366 8.16675 2.33366C4.94341 2.33366 2.33341 4.94366 2.33341 8.16699C2.33341 11.3895 4.94341 14.0003 8.16675 14.0003C9.68377 14.0027 11.1417 13.4121 12.2292 12.3545L12.3542 12.2295Z" fill="currentColor"/>
            </svg>
          </div>
          <input type="search" class="symbol-searchbar__input" placeholder="Search jobs...">
          <div class="symbol-searchbar__icon symbol-searchbar__icon--right"></div>
        </div>
        <h6 class="margins">Filled</h6>
        <div class="symbol-searchbar">
          <div class="symbol-searchbar__icon symbol-searchbar__icon--left">
            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M14.0259 12.8478L17.5951 16.4162L16.4159 17.5953L12.8476 14.0262C11.5199 15.0905 9.86842 15.6694 8.16675 15.667C4.02675 15.667 0.666748 12.307 0.666748 8.16699C0.666748 4.02699 4.02675 0.666992 8.16675 0.666992C12.3067 0.666992 15.6667 4.02699 15.6667 8.16699C15.6692 9.86866 15.0903 11.5201 14.0259 12.8478ZM12.3542 12.2295C13.4118 11.1419 14.0025 9.68402 14.0001 8.16699C14.0001 4.94366 11.3892 2.33366 8.16675 2.33366C4.94341 2.33366 2.33341 4.94366 2.33341 8.16699C2.33341 11.3895 4.94341 14.0003 8.16675 14.0003C9.68377 14.0027 11.1417 13.4121 12.2292 12.3545L12.3542 12.2295Z" fill="currentColor"/>
            </svg>
          </div>
          <input type="search" class="symbol-searchbar__input" value="jim.person" placeholder="Search jobs...">
          <div class="symbol-searchbar__icon symbol-searchbar__icon--right"></div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="symbol-section symbol-section--small">
  <div class="container overflow-visible">
    <div class="separator"></div>
  </div>
</section>

<section class="symbol-section">
  <div class="container overflow-visible">
    <div class="cols">
      <div class="col is-12">
        <h3 class="no-margin">Info in numbers  <br /><code>.symbol-info-numbers</code></h3>
        <h6 class="margins">Default</h6>
      </div>
      <div class="col is-12 is-6-md">
        <div class="symbol-info-numbers">
          <div class="symbol-info-numbers__container">
            <h2 class="symbol-info-numbers__number">75</h2>
            <span class="symbol-info-number__text">Vacancies</span>
          </div>
          <div class="symbol-info-numbers__container">
            <h2 class="symbol-info-numbers__number">8</h2>
            <span class="symbol-info-number__text">Vacancies</span>
          </div>
          <div class="symbol-info-numbers__container">
            <h2 class="symbol-info-numbers__number">75</h2>
            <span class="symbol-info-number__text">Vacancies</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="symbol-section symbol-section--small">
  <div class="container overflow-visible">
    <div class="separator"></div>
  </div>
</section>

