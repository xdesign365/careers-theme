<?php

/**
 * Create Feed settings page.
 */
if ( ! class_exists('WorkableFeedSettings') ) {
    class WorkableFeedSettings {

        /**
         * WorkableFeedSettings constructor.
         */
        public function __construct() {
            add_action('admin_menu', array($this,'workable_feed_settings_menu'));
            add_action('admin_init', array($this, 'workable_feed_settings_register_settings'));
        }

        /**
         * Workable Feed settings menu.
         */
        public function workable_feed_settings_menu() {
            add_menu_page(
                __('Workable Feed', 'workable-feed'),
                __('Workable Feed', 'workable-feed'),
                'manage_options',
                'workable-feed',
                array($this, 'workable_feed_settings_menu_page'),
                WP_PLUGIN_URL . '/workable/assets/icons/workable.svg',
                70
            );
        }

        /**
         * Workable Feed register settings.
         */
        public function workable_feed_settings_register_settings() {
            // Add option to Database
            add_option('api_key', '', '', 'yes');
            // Register settings that this form is allowed to update.
            register_setting('workable-feed', 'api_key');
        }

        public function workable_feed_settings_menu_page() {
            
            // Check if user have permission.
            if (!current_user_can('manage_options')) {
                wp_die(__("You don't have access to this page"));
            }
            ?>

            <div class="wrap">
            <h2>Workable Feed</h2>
                <?php
                // print_r((new WorkableFeed())->get_body());
                ?>
            <form method="post" action="options.php">
                <table class="form-table">
                    <tr>
                        <th scope="row">API Key</th>
                        <td>
                            <label>
                                <input type="password" name="api_key" value="<?php echo esc_attr( get_option('api_key') ); ?>" />
                            </label>
                        </td>
                    </tr>
                </table>
                <?php
                settings_fields("workable-feed");
                do_settings_sections("workable-feed");
                submit_button('Save');
                ?>
            </form>
            </div>

            <?php
        }
    }

    $workableFeedSettings = new WorkableFeedSettings();
}