<?php

if ( ! class_exists('WorkableAjax') ) {
    class WorkableAjax {

        public function __construct() {
            $this->enqueue_scripts();
            $this->setup_ajax_handlers();
        }

        private function enqueue_scripts() {
            wp_enqueue_script(
                'ajax-script',
                plugin_dir_url( dirname( __FILE__ ) ) . 'assets/js/filters.js',
                [],
                '1.0',
                TRUE
            );
            wp_localize_script(
                'ajax-script',
                'workable_ajax',
                ['ajax_url' => admin_url( 'admin-ajax.php' )]
            );
        }

        public function setup_ajax_handlers() {
            add_action(
                'wp_ajax_get_jobs',
                [$this, 'get_jobs']
            );
            add_action(
                'wp_ajax_nopriv_get_jobs',
                [$this, 'get_jobs']
            );
        }

        public function get_jobs() {

            $parameters = [];

            if (isset($_POST['search_string']) && strlen($_POST['search_string']) > 0) {
                $parameters['search_string'] = $_POST['search_string'];
            } else {
                $parameters['search_string'] = NULL;
            }
            if (isset($_POST['location'])) {
                $parameters['location'] = $_POST['location'];
            } else {
                $parameters['location'] = NULL;
            }
            if (isset($_POST['department'])) {
                $parameters['department'] = $_POST['department'];
            } else {
                $parameters['department'] = NULL;
            }
            if (isset($_POST['work_type'])) {
                $parameters['work_type'] = $_POST['work_type'];
            } else {
                $parameters['work_type'] = NULL;
            }
            if (isset($_POST['remote_only'])) {
                $parameters['remote_only'] = $_POST['remote_only'];
            }

            $body = (new WorkableFeed())->get_body(
                $parameters['search_string'],
                $parameters['location'],
                $parameters['department'],
                $parameters['work_type'],
                $parameters['remote_only']
            );

            echo json_encode(['body' => esc_html($body)]);
            die;

        }

    }

    $workableAjax = new WorkableAjax();

}