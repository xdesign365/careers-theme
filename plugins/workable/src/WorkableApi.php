<?php

if ( ! class_exists('WorkableApi') ) {
    class WorkableApi {
        
        public function __construct() {
        }

        public function workable_feed_filters() {
            $token = get_option('api_key');
            $filters = [];

            // Get departments.
            // $curl_departments = curl_init();
            // curl_setopt_array($curl_departments, array(
            //     CURLOPT_URL => 'https://xdesign.workable.com/api/accounts/xdesign/departments',
            //     CURLOPT_RETURNTRANSFER => true,
            //     CURLOPT_ENCODING => '',
            //     CURLOPT_MAXREDIRS => 10,
            //     CURLOPT_TIMEOUT => 0,
            //     CURLOPT_FOLLOWLOCATION => true,
            //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //     CURLOPT_CUSTOMREQUEST => 'GET',
            //     CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5',
            // ));
            // $departments = curl_exec($curl_departments);
            // curl_close($curl_departments);
            $departments = file_get_contents('https://careers.xdesign.com/wp-content/themes/xdesign/workable-departments.json', true);
            $filters['departments'] = json_decode($departments, true);

            // Get locations.
            // $curl_jobs = curl_init();
            // curl_setopt_array($curl_jobs, array(
            //     CURLOPT_URL => 'https://xdesign.workable.com/spi/v3/jobs?state=published&include_fields=employment_type,description',
            //     CURLOPT_RETURNTRANSFER => true,
            //     CURLOPT_ENCODING => '',
            //     CURLOPT_MAXREDIRS => 10,
            //     CURLOPT_TIMEOUT => 0,
            //     CURLOPT_FOLLOWLOCATION => true,
            //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //     CURLOPT_CUSTOMREQUEST => 'GET',
            //     CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5',
            //     CURLOPT_HTTPHEADER => array(
            //         'Authorization: Bearer ' . $token
            //     ),
            // ));
            // $jobs = json_decode(curl_exec($curl_jobs), true);
            // curl_close($curl_jobs);
            $jobsData = file_get_contents('https://careers.xdesign.com/wp-content/themes/xdesign/workable-jobs.json', true);
            $jobs = json_decode($jobsData, true);

            if (isset($jobs['jobs'])) {
                $i = 0;
                foreach ($jobs['jobs'] as $item) {
                    $filters['locations'][$i]['name'] = isset($item['location']['country']) ? $item['location']['country'] : '';
                    $filters['locations'][$i]['name'] .= isset($item['location']['region']) && strlen($item['location']['region']) > 1 ? ', ' . $item['location']['region'] : '';
                    $filters['locations'][$i]['name'] .= isset($item['location']['city']) && strlen($item['location']['city']) > 1 ? ', ' . $item['location']['city'] : '';
                    $filters['locations'][$i]['country'] = isset($item['location']['country']) ? $item['location']['country'] : '';
                    $filters['locations'][$i]['region'] = isset($item['location']['region']) ?  $item['location']['region'] : '';
                    $filters['locations'][$i]['city'] = isset($item['location']['city']) ?  $item['location']['city'] : '';
                    $filters['employment_type'][$i] = isset($item['employment_type']) ?  $item['employment_type'] : '';
                    $i++;
                }
            }
            $filters['locations'] = array_unique($filters['locations'], SORT_REGULAR);
            $filters['employment_type'] = array_unique($filters['employment_type']);
            asort($filters['locations']);

            return $filters;
        }



        public function workable_feed_jobs() {
            // $token = get_option('api_key');

            // if (isset($token)) {

            //     $curl = curl_init();

            //     curl_setopt_array($curl, array(
            //         CURLOPT_URL => 'https://xdesign.workable.com/spi/v3/jobs?state=published&include_fields=employment_type',
            //         CURLOPT_RETURNTRANSFER => true,
            //         CURLOPT_ENCODING => '',
            //         CURLOPT_MAXREDIRS => 10,
            //         CURLOPT_TIMEOUT => 0,
            //         CURLOPT_FOLLOWLOCATION => true,
            //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //         CURLOPT_CUSTOMREQUEST => 'GET',
            //         CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5',
            //         CURLOPT_HTTPHEADER => array(
            //             'Authorization: Bearer ' . $token
            //         ),
            //     ));
            // }

            $jobsData = file_get_contents('https://careers.xdesign.com/wp-content/themes/xdesign/workable-jobs.json', true);

            // $response = curl_exec($curl);

            // curl_close($curl);

            $response = json_decode($jobsData , true)['jobs'];

            return array_reverse($response);
        }

    }
    $workableApi = new WorkableApi();
}