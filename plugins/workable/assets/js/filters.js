// Array with current selected filters
let filtersArray = [];

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

window.addEventListener("load", function () {
  // Main search input
  let main_search = document.querySelector(".workable-api__search input");
  // All dropdown items
  let array = Array.from(
    document.querySelectorAll(".nice-select .nice-select-dropdown ul li")
  );
  // Remote switcher
  let remote_switcher = document.querySelector(
    ".workable-api__remote-switcher input"
  );

  // Function to get current values of filters
  function checkFiltersStatus() {
    // Empty array to collect current filters
    filtersArray = [];

    // Pushing the search filter value
    filtersArray.push(main_search.value);

    // Pushing the dropdowns filter values
    let spans_array = Array.from(
      document.querySelectorAll(".nice-select .current")
    );
    spans_array.forEach((elem, index) => {
      // Statement, if the filter does include one of default values it will add it as '' empty string.
      if (
        elem.textContent.includes("Location") ||
        elem.textContent.includes("Department") ||
        elem.textContent.includes("Work Type")
      ) {
        filtersArray.push(null);
      } else {
        filtersArray.push(elem.textContent);
      }
    });

    // Pushing the remote switcher value
    if (remote_switcher.checked) {
      filtersArray.push(true);
    } else {
      filtersArray.push("");
    }

    // console.log(filtersArray);
  }

  function searchBar() {
    checkFiltersStatus();
    if (main_search.value.length === 0 || main_search.value.length > 2) {
      get_jobs(
        filtersArray[0],
        filtersArray[1],
        filtersArray[2],
        filtersArray[3],
        filtersArray[4]
      );
    }
  }

  const searchHandler = debounce(searchBar, 500);

  // Main search bar
  if (main_search != null) {
    main_search.addEventListener("input", searchHandler);
  }

  // Adding function to switcher on click event
  if (remote_switcher != null) {
    remote_switcher.addEventListener("click", function () {
      checkFiltersStatus();
      get_jobs(
        filtersArray[0],
        filtersArray[1],
        filtersArray[2],
        filtersArray[3],
        filtersArray[4]
      );
    });
  }
  // Adding event on click on all dropdown filter items
  for (let i = 0; i < array.length; i++) {
    array[i].addEventListener("click", function () {
      checkFiltersStatus();
      get_jobs(
        filtersArray[0],
        filtersArray[1],
        filtersArray[2],
        filtersArray[3],
        filtersArray[4]
      );
    });
  }

  const niceSelect = document.querySelectorAll(".nice-select");

  niceSelect.forEach((el) =>
    el.addEventListener("click", (event) => {
      console.log(event.target.parentElement);

      setTimeout(() => {
        event.target.parentElement.classList.toggle("open");
      }, 10);
    })
  );

  // Reset filters button
  let reset_button = document.querySelector(
    ".workable-api__reset-filters-wrapper .workable-api__reset-filters-button"
  );

  // Setting default values for the filters
  if (reset_button != null) {
    reset_button.addEventListener("click", function () {
      document.querySelector(".workable-api__search input").value = "";
      document.querySelector(
        ".workable-api__location .nice-select span"
      ).innerHTML = "Location";
      document.querySelector(
        ".workable-api__department .nice-select span"
      ).innerHTML = "Department";
      document.querySelector(
        ".workable-api__work-type .nice-select span"
      ).innerHTML = "Work Type";
      document.querySelector(
        ".workable-api__remote-switcher .remote-checkbox"
      ).checked = false;

      const elNiceSelects = document.querySelectorAll(
        ".nice-select-dropdown .option"
      );
      [].forEach.call(elNiceSelects, function (el) {
        if (el.classList.contains("selected")) {
          el.classList.remove("selected");
        }
      });

      filtersArray = [];
      // console.log(filtersArray);
      get_jobs(
        filtersArray[0],
        filtersArray[1],
        filtersArray[2],
        filtersArray[3],
        filtersArray[4]
      );
    });
  }

  // console.log(reset_button);
});

function unEscape(htmlStr) {
  htmlStr = htmlStr.replace(/&lt;/g, "<");
  htmlStr = htmlStr.replace(/&gt;/g, ">");
  htmlStr = htmlStr.replace(/&quot;/g, '"');
  htmlStr = htmlStr.replace(/&#39;/g, "'");
  htmlStr = htmlStr.replace(/&amp;/g, "&");

  return htmlStr;
}

function get_jobs(
  search_string = null,
  location = null,
  department = null,
  work_type = null,
  remote_only = null
) {
  let body = document.querySelector("body");
  body.classList.add("body-workable");
  //   console.log(body);

  let loader = document.querySelector(".jobs-loader");
  loader.classList.add("jobs-loader-open");

  let jobs_content = document.querySelector(".workable-api__jobs-content");
  jobs_content.innerHTML = "";

  const form = new FormData();
  form.append("action", "get_jobs");

  if (search_string !== null && search_string !== undefined) {
    form.append("search_string", search_string);
  }
  if (location !== null && location !== undefined) {
    form.append("location", location);
  }
  if (department !== null && department !== undefined) {
    form.append("department", department);
  }
  if (work_type !== null && work_type !== undefined) {
    form.append("work_type", work_type);
  }
  if (remote_only !== null && remote_only !== undefined) {
    form.append("remote_only", remote_only);
  }

  const params = new URLSearchParams(form);

  fetch(workable_ajax.ajax_url, {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Cache-Control": "no-cache",
    },
    body: params,
  })
    .then((response) => {
      return response.json();
    })
    .then((response) => {
      // console.log(response['body']);
      // console.log(unEscape(response['body'].toString()));

      let no_jobs_msg = document.querySelector(".workable-api__not-found-msg");

      jobs_content.innerHTML = unEscape(response["body"].toString());
      loader.classList.remove("jobs-loader-open");

      ScrollReveal().sync();

      if (jobs_content.innerHTML === "") {
        no_jobs_msg.classList.add("msg-open");
      } else {
        no_jobs_msg.classList.remove("msg-open");
      }
    })
    .catch((err) => {
      console.log(err);
    });
}
