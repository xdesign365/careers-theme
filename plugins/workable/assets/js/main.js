// Initialization of the NiceSelect2 libary for dropdowns

if(document.getElementById("location-select") != null ) {
    NiceSelect.bind(document.getElementById("location-select"),{
        placeholder: 'Location',
    });
}
if(document.getElementById("department-select") != null ) {
    NiceSelect.bind(document.getElementById("department-select"),{
        searchable: true,
        placeholder: 'Department',
    });
}
if(document.getElementById("work-type-select") != null ) {
    NiceSelect.bind(document.getElementById("work-type-select"),{
        placeholder: 'Work Type',
    });
}
