<?php
/**
 * Plugin Name: Workable
 * Plugin URI: https://www.xdesign.com
 * Description: Plugin used to connect Workable API v3 https://workable.readme.io/docs/jobs
 * Version: 1.0
 * Author: xDesign
 * Author URI: https://www.xdesign.com
 */
// Originally built by FORGE - updated to work with v3 api

require_once 'src/WorkableFeedSettings.php';
require_once 'src/WorkableApi.php';
require_once 'src/WorkableAjax.php';		


if (!class_exists('WorkableFeed')) {
    class WorkableFeed
    {
        public function __construct()
        {
            add_shortcode('WorkableFeed', array($this, 'shortcode'));
            $this->enqueue_scripts();
        }

        public function shortcode() {

            // Workable API main wrapper
            $output = '<div class="workable-api container">';

            // Workable API main content wrapper
            $output .= '<div class="workable-api__main-content">';

            $output .= $this->get_filters();

            // Loader
            $output .= '<div class="jobs-loader"></div>';
            // Message if none of the job is found
            $output .= '<p class="workable-api__not-found-msg">Sorry, no jobs matching your search criteria could be found.</p>';

            $output .= '<div class="workable-api__jobs-content">';



            $output .= $this->get_body();

            // Closing tag of jobs content wrapper
            $output .= '</div>';

            // $output .= '<div class="workable-api__cta-wrapper">';
            // $output .= '<a rel="noopener" href="https://apply.workable.com/xdesign/" target="_blank" class="workable-api__cta">Show More</a>';
            // $output .= '</div>';

            // Closing tag of "workable-api__main-content"
            $output .= '</div>';
            // Closing tag of "workable-api" main wrapper
            $output .= '</div>';

            return $output;
        }

        public function get_filters($search_string = NULL,$location = NULL,
                                    $department = NULL, $work_type = NULL,
                                    $remote_only = FALSE) {

            $workable_api_filters = (new WorkableApi)->workable_feed_filters();

            $filters = '';

            // Filters Wrapper
            $filters .= '<div class="workable-api__filters">';

            // Search container & input
            $filters .= '<div class="workable-api__search">';
            $filters .= '<label><span class="sr-only">Search jobs at xDesign</span><input placeholder="Search jobs..." type="search" id="-mains-earch" name="main-search"></label>';
            $filters .= '</div>';

            // Locations filter
            $filters .= '<div class="workable-api__select workable-api__location">';
            $filters .= '<div>';

            // Location list
            $filters .= '<label for="location-select">Location';
            $filters .= '<select name="location" id="location-select">';
            // Loop to get location names
            foreach ($workable_api_filters['locations'] as $location) {

                // Location list item
                $filters .= '<option>';
                // Location name
                $filters .= $location['name'];
                $filters .= '</option>';
            }
            $filters .= '</select>';
            $filters .= '</label>';

            $filters .= '</div>';
            $filters .= '</div>';

            // Department filter
            $filters .= '<div class="workable-api__select workable-api__department">';
            $filters .= '<div>';


            // Department list
            $filters .= '<label for="department-select">Department';
            $filters .= '<select name="department" id="department-select">';

            // Loop to get department names
            foreach ($workable_api_filters['departments'] as $department) {

                // Department list item
                $filters .= '<option>';
                // Department name
                $filters .= $department['name'];
                $filters .= '</option>';
            }
            $filters .= '</select>';
            $filters .= '</label>';

            $filters .= '</div>';
            $filters .= '</div>';


            // Work Type filter
            $filters .= '<div class="workable-api__select workable-api__work-type">';
            $filters .= '<div>';

            // Location list
            $filters .= '<label for="work-type-select">Work Type';
            $filters .= '<select name="work-type" id="work-type-select">';
            // Loop to get work types names
            foreach ($workable_api_filters['employment_type'] as $work_type) {

                // Work type list item
                $filters .= '<option>';
                // Location name
                $filters .= $work_type;
                $filters .= '</option>';
            }
            $filters .= '</select>';
            $filters .= '</label>';

            $filters .= '</div>';
            $filters .= '</div>';


            // Remote Switcher
            $filters .= '<div class="workable-api__remote-switcher">';
            $filters .= '<input class="remote-checkbox" name="remote" id="remote" type="checkbox">';
            $filters .= '<label for="remote">Remote Only</label>';
            $filters .= '<span>Remote Only</span>';
            $filters .= '</div>';

            // Reset button
            $filters .= '<div class="workable-api__reset-filters-wrapper">';
                $filters .= '<a class="workable-api__reset-filters-button"></a>';
            $filters .= '</div>';

            // Closing tag of "workable-api__filters"
            $filters .= '</div>';

            return $filters;

        }

        public function get_body($search_string = NULL, $location = NULL,
                                 $department = NULL, $work_type = NULL,
                                 $remote_only = FALSE) {

            $workable_api_jobs = (new WorkableApi)->workable_feed_jobs();

            $content = '';

            // Jobs content wrapper
            $i = 0;
            foreach($workable_api_jobs as $job) {

                 if ($search_string !== NULL || strlen($search_string) > 0) {
                    if(stristr($job['title'], $search_string) === FALSE && stristr($job['description'], $search_string) === FALSE) {
                        continue;
                    }
                 }

                if ($location !== NULL) {
                    $job_location = isset($job['location']['country']) ? $job['location']['country'] : '';
                    $job_location .= isset($job['location']['region']) && strlen($job['location']['region']) > 1 ? ', ' . $job['location']['region'] : '';
                    $job_location .= isset($job['location']['city']) && strlen($job['location']['city']) > 1 ? ', ' . $job['location']['city'] : '';

                    if (strpos(strtolower($job_location), strtolower($location)) === FALSE) {
                        continue;
                    }
                }

                if(get_field('department_select')) {
                    $acf_selected_department = get_field('department_select');
                }

                if ($department !== NULL) {
                    if (isset($job['department'])) {
                        if (strpos(strtolower($job['department']), strtolower(str_replace('–', '-', $department))) === FALSE) {
                            continue;
                        }
                    }
                } elseif (isset($acf_selected_department) && $acf_selected_department != 'Unset') {
                    if (isset($job['department'])) {
                        if (strpos(strtolower($job['department']), strtolower($acf_selected_department)) === FALSE) {
                            continue;
                        }
                    }
                }

                if ($work_type !== NULL) {
                    if (isset($job['employment_type']) && $job['employment_type'] != $work_type) {
                        continue;
                    }
                }

                if ($remote_only == TRUE) {
                    if (isset($job['location']['telecommuting']) && $job['location']['telecommuting'] === false) {
                        continue;
                    }
                }

                // Creating job to be clickable
                $content .= '<a rel="noopener" href="';
                $content .= $job['url'];
                $content .= '" target="_blank">';

                // Single job wrapper
                $content .= '<div class="workable-api__job">';

                // Field for job information and date
                $content .= '<div class="workable-api__job-information">';

                $content .= '<h2 class="workable-api__job-title">';
                $content .= $job['title'];
                $content .= '</h2>';

                $content .= '<p class="workable-api__job-post-date">';
                $content .= date('Y-m-d', strtotime($job['created_at']));
                $content .= "</p>";

                $content .= '</div>';

                // Field for job location
                $content .= '<div class="workable-api__job-location">';

                $content .= isset($job['location']['region']) && strlen($job['location']['region']) > 0 ? '<h3 class="workable-api__city">' : '';
                $content .= $job['location']['city'];
                $content .= isset($job['location']['region']) && strlen($job['location']['region']) > 0 ? ', ' . $job['location']['region'] : '';
                $content .= isset($job['location']['region']) && strlen($job['location']['region']) > 0 ? '</h3>' : '';

                $content .= '<p class="workable-api__country">';
                $content .= $job['location']['country'];
                $content .= '</p>';

                $content .= '</div>';

                // Field for job position
                $content .= '<div class="workable-api__job-position">';
                $content .= '<p class="workable-api__position">';
                $content .= $job['department'];
                $content .= '</p>';
                $content .= '</div>';

                // Field for work type
                $content .= '<div class="workable-api__job-duration">';
                $content .= '<p class="workable-api__duration">';
                $content .= $job['employment_type'];
                $content .= '</p>';
                $content .= '</div>';

                $content .= '</div>';

                // Closing tag of anchor for job
                $content .= '</a>';

                $i++;

                // if($i == 10) {
                //     break;
                // }
            }

            return $content;

        }

        public function enqueue_scripts() {
            wp_enqueue_style('nice-select-2-style', WP_PLUGIN_URL . '/workable/assets/nice-select2/css/nice-select2.css', '', '', false);
            wp_enqueue_style('nice-select-2-style-main', WP_PLUGIN_URL . '/workable/assets/nice-select2/css/style.css', '', '', false);
            wp_enqueue_script('nice-select-2-script', WP_PLUGIN_URL . '/workable/assets/nice-select2/js/nice-select2.js', '', '', true);
            wp_enqueue_script('workable-main-script', WP_PLUGIN_URL . '/workable/assets/js/main.js', '', '', true);
        }

    }

    $workableApi = new WorkableFeed();





    // $workable_api_filters = (new WorkableApi)->workable_feed_filters();


    // $departmentsArray = $workable_api_filters['departments'];

    // $acf_select = ['Unset' => 'Unset'];

    // foreach($departmentsArray as $single_department) {
    //     $acf_select[$single_department['name']] = $single_department['name'];
    // }			

    // if( function_exists('acf_add_local_field_group') ):

    //     acf_add_local_field_group(array(
    //         'key' => 'group_61b72e0bcab9c',
    //         'title' => 'Block: Workable API',
    //         'fields' => array(
    //             array(
    //                 'key' => 'field_61b72f753ebc5',
    //                 'label' => 'Department select',
    //                 'name' => 'department_select',
    //                 'type' => 'select',
    //                 'instructions' => 'Set a default department/category of jobs to initially show.',
    //                 'required' => 0,
    //                 'conditional_logic' => 0,
    //                 'wrapper' => array(
    //                     'width' => '',
    //                     'class' => '',
    //                     'id' => '',
    //                 ),
    //                 'choices' => $acf_select,
    //                 'default_value' => false,
    //                 'allow_null' => 0,
    //                 'multiple' => 0,
    //                 'ui' => 0,
    //                 'return_format' => 'label',
    //                 'ajax' => 0,
    //                 'placeholder' => '',
    //             ),
    //         ),
    //         'location' => array(
    //             array(
    //                 array(
    //                     'param' => 'block',
    //                     'operator' => '==',
    //                     'value' => 'acf/workable-api',
    //                 ),
    //             ),
    //         ),
    //         'menu_order' => 0,
    //         'position' => 'normal',
    //         'style' => 'default',
    //         'label_placement' => 'top',
    //         'instruction_placement' => 'label',
    //         'hide_on_screen' => '',
    //         'active' => true,
    //         'description' => '',
    //         'show_in_rest' => 0,
    //     ));
        
    //     endif;		
}
