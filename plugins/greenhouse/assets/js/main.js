// Initialization of the NiceSelect2 libary for dropdowns

if (document.getElementById("location-select") != null) {
  NiceSelect.bind(document.getElementById("location-select"), {
    placeholder: "Location",
  });
}
if (document.getElementById("department-select") != null) {
  NiceSelect.bind(document.getElementById("department-select"), {
    searchable: true,
    placeholder: "Department",
  });

  applyGroupingToNiceSelectDropdown("department-select");
}
if (document.getElementById("work-type-select") != null) {
  NiceSelect.bind(document.getElementById("work-type-select"), {
    placeholder: "Employment Type",
  });
}

// Deparmtents are tied in levels, this function reads the original dropdown and applies it to the new custom dropdown
function applyGroupingToNiceSelectDropdown(selectElementId) {
  var selectDropdownEl = document.getElementById(selectElementId);
  var selectDropdownOptionEls = selectDropdownEl.querySelectorAll("option");
  var niceSelectDropdownEl = selectDropdownEl.nextElementSibling;

  for (var i = 0; i < selectDropdownOptionEls.length; i++) {
    console.log(selectDropdownOptionEls[i].className);
    var classNames = selectDropdownOptionEls[i].className;
    var value = selectDropdownOptionEls[i].value;

    if (niceSelectDropdownEl) {
      var matchingOption = niceSelectDropdownEl.querySelector(
        "li[data-value='" + value + "']"
      );

      if (matchingOption) {
        matchingOption.classList.add(classNames);
      }
    }
  }
}
