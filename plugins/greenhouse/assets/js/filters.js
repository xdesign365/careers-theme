// Array with current selected filters
let filtersArray = [];
let selectors = {
  nice_select_span_department: ".greenhouse-api__department .nice-select span",
  nice_select_span_locations: ".greenhouse-api__location .nice-select span",
  nice_select_span_employment_type:
    ".greenhouse-api__work-type .nice-select span",
  search_input: ".greenhouse-api__search input",
  nice_select: ".nice-select",
};

let classNames = {
  nice_select_span_option_chosen: "current--chosen",
};

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

window.addEventListener("load", function () {
  // Main search input
  let main_search = document.querySelector(".greenhouse-api__search input");
  // All dropdown items
  let dropdownOptionEls = Array.from(
    document.querySelectorAll(".nice-select .nice-select-dropdown ul li")
  );
  // Remote switcher
  let remote_switcher = document.querySelector(
    ".greenhouse-api__remote-switcher input"
  );

  // Function to get current values of filters
  function checkFiltersStatus() {
    // Empty array to collect current filters
    filtersArray = [];

    // Pushing the search filter value
    filtersArray.push(main_search.value);

    // Pushing the dropdowns filter values
    let dropdownElements = Array.from(
      document.querySelectorAll(".greenhouse-api__filters .nice-select")
    );
    dropdownElements.forEach((elem, index) => {
      var selectedOption = elem.querySelector(".list .option.selected");
      // Statement, if the filter does include one of default values it will add it as '' empty string.
      if (selectedOption) {
        filtersArray.push(selectedOption.getAttribute("data-value"));
      } else {
        filtersArray.push(null);
      }
    });
  }

  function searchBar() {
    checkFiltersStatus();
    if (main_search.value.length === 0 || main_search.value.length > 2) {
      get_jobs(
        filtersArray[0],
        filtersArray[1],
        filtersArray[2],
        filtersArray[3]
      );
    }
  }

  const searchHandler = debounce(searchBar, 500);

  // Main search bar
  if (main_search != null) {
    main_search.addEventListener("input", searchHandler);
  }

  // Adding event on click on all dropdown filter items
  for (let i = 0; i < dropdownOptionEls.length; i++) {
    dropdownOptionEls[i].addEventListener("click", function (el) {
      checkFiltersStatus();

      let parentNiceSelect = el.target.closest(selectors.nice_select);
      let selectCurrentSpan = parentNiceSelect.querySelector("span.current");
      selectCurrentSpan.classList.add(
        classNames.nice_select_span_option_chosen
      );

      get_jobs(
        filtersArray[0],
        filtersArray[1],
        filtersArray[2],
        filtersArray[3]
      );
    });
  }

  const niceSelect = document.querySelectorAll(".nice-select");

  niceSelect.forEach((el) =>
    el.addEventListener("click", (event) => {
      setTimeout(() => {
        event.target.parentElement.classList.toggle("open");
      }, 10);
    })
  );

  // Reset filters button
  let reset_button = document.querySelector(
    ".greenhouse-api__reset-filters-wrapper .greenhouse-api__reset-filters-button"
  );

  // Setting default values for the filters
  if (reset_button != null) {
    reset_button.addEventListener("click", function () {
      document.querySelector(selectors.search_input).value = "";
      var locationNiceSelect = document.querySelector(
        selectors.nice_select_span_locations
      );
      var departmentNiceSelect = document.querySelector(
        selectors.nice_select_span_department
      );
      var employmentTypeNiceSelect = document.querySelector(
        selectors.nice_select_span_employment_type
      );

      locationNiceSelect.innerHTML = "Location";
      locationNiceSelect.classList.remove(
        classNames.nice_select_span_option_chosen
      );
      departmentNiceSelect.innerHTML = "Department";
      departmentNiceSelect.classList.remove(
        classNames.nice_select_span_option_chosen
      );
      employmentTypeNiceSelect.innerHTML = "Employment Type";
      employmentTypeNiceSelect.classList.remove(
        classNames.nice_select_span_option_chosen
      );

      const elNiceSelects = document.querySelectorAll(
        ".nice-select-dropdown .option"
      );
      [].forEach.call(elNiceSelects, function (el) {
        if (el.classList.contains("selected")) {
          el.classList.remove("selected");
        }
      });

      filtersArray = [];

      get_jobs(
        filtersArray[0],
        filtersArray[1],
        filtersArray[2],
        filtersArray[3]
      );
    });
  }
});

function unEscape(htmlStr) {
  htmlStr = htmlStr.replace(/&lt;/g, "<");
  htmlStr = htmlStr.replace(/&gt;/g, ">");
  htmlStr = htmlStr.replace(/&quot;/g, '"');
  htmlStr = htmlStr.replace(/&#39;/g, "'");
  htmlStr = htmlStr.replace(/&amp;/g, "&");

  return htmlStr;
}

function get_jobs(
  search_string = null,
  location = null,
  department = null,
  work_type = null
) {
  let body = document.querySelector("body");
  body.classList.add("body-greenhouse");

  let loader = document.querySelector(".jobs-loader");
  loader.classList.add("jobs-loader-open");

  let jobs_content = document.querySelector(".greenhouse-api__jobs-content");
  jobs_content.innerHTML = "";

  const form = new FormData();
  form.append("action", "get_jobs");

  if (search_string !== null && search_string !== undefined) {
    form.append("search_string", search_string);
  }
  if (location !== null && location !== undefined) {
    form.append("location", location);
  }

  if (department !== null && department !== undefined) {
    form.append("department", department);
  }
  if (work_type !== null && work_type !== undefined) {
    form.append("work_type", work_type);
  }

  const params = new URLSearchParams(form);

  fetch(greenhouse_ajax.ajax_url, {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Cache-Control": "no-cache",
    },
    body: params,
  })
    .then((response) => {
      return response.json();
    })
    .then((response) => {
      let no_jobs_msg = document.querySelector(
        ".greenhouse-api__not-found-msg"
      );

      jobs_content.innerHTML = unEscape(response["body"].toString());
      loader.classList.remove("jobs-loader-open");

      ScrollReveal().sync();

      if (jobs_content.innerHTML === "") {
        no_jobs_msg.classList.add("msg-open");
      } else {
        no_jobs_msg.classList.remove("msg-open");
      }
    })
    .catch((err) => {
      console.log(err);
    });
}
