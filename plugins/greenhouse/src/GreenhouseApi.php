<?php

if ( ! class_exists('GreenhouseApi') ) {
    class GreenhouseApi {


        // 4325575101 is the id for the field 'Employment Type'
        private $employment_type_field_id = 4325575101;
        
        public function __construct() {
        }

        public function greenhouse_feed_filters() {
            $token = get_option('api_key');
            $filters = [];
            $filters['locations'] = [];
            $filters['departments'] = [];
            $filters['employment_type'] = [];
        

            $filters['locations'] = $this->filter_locations();
            $filters['employment_type'] = $this->get_all_unique_employment_types();
            $filters['departments'] = $this->generate_tiered_departments();

            return $filters;
        }


        public function greenhouse_feed_jobs() {
            $jobs = $this->greenhouse_get_jobs ();

            return array_reverse($jobs);
        }

        // The offices json contains some non-standard locations like Remote for example
        // We filter these out by checking if they actually contain a location
        public function filter_real_locations () {
            $offices = $this->greenhouse_get_offices ();
            
            $tempLocations = [];

            foreach ($offices as $office) {
                if ($office['location'] !== NULL) {
                    array_push($tempLocations, $office['name']);
                }
            }
            asort($tempLocations);
            return $tempLocations;
        }

        // Adds Remote as an option to filter
        private function filter_locations () {
            $realOffices = $this->filter_real_locations();
            $tempLocationOptions = $realOffices;

            array_push($tempLocationOptions, 'Remote');
            asort($tempLocationOptions);
            return $tempLocationOptions;
        }

        // Departments are in a flat structure but are actually grouped in parent and child relation
        // This function grabs all available departments and sorts them into their groups 
        public function generate_tiered_departments () {
            $departments = $this->greenhouse_get_departments ();
            $filteredDepartments = $this->sort_all_unique_parent_and_child_departments($departments);
            $tiered_departments = [];

            foreach($filteredDepartments['parentDepartments'] as $parent_department) {
                $temp_parent_department = [];
                $temp_parent_department['id'] = $parent_department['id'];
                $temp_parent_department['name'] = $parent_department['name'];
                $temp_parent_department['sub_departments'] = [];
                $temp_parent_department['job_count'] = count($parent_department['jobs']);

                foreach($filteredDepartments['childDepartments'] as $child_department) {
                    if ($child_department['parent_id'] == $parent_department['id']) {
                        $temp_child_department = [];
                        $temp_child_department['id'] = $child_department['id'];
                        $temp_child_department['name'] = $child_department['name'];
                        $temp_child_department['job_count'] = count($child_department['jobs']);
                        array_push($temp_parent_department['sub_departments'], $temp_child_department);
                        $temp_parent_department['job_count'] += $temp_child_department['job_count'];
                    }
                }

                if ($temp_parent_department['job_count'] > 0) {
                    array_push($tiered_departments, $temp_parent_department);
                }
            }

            return $tiered_departments;
        }

        // creates two lists for parent function generate_tiered_departments to create new structure
        private function sort_all_unique_parent_and_child_departments ($departments) {
            $tempParentDepartments = [];
            $tempChildDepartments = [];
            $filteredDepartments = [];

            foreach ($departments as $department) {
                if (isset($department['jobs'])) {
                    if($department['parent_id'] == null) {
                        array_push($tempParentDepartments, $department);
                    } else {
                        if (count($department['jobs']) > 0) {
                            array_push($tempChildDepartments, $department);
                        }
                    }
              }
            }

            $filteredDepartments['parentDepartments'] = $tempParentDepartments;
            $filteredDepartments['childDepartments'] = $tempChildDepartments;

            return $filteredDepartments;
        }


        private function get_all_unique_employment_types () {
            $departments = $this->greenhouse_get_departments ();
            $employmentTypes = [];

            foreach ($departments as $department) {
                if (!isset($department['jobs'])) {
                    continue;
                }

                foreach($department['jobs'] as $job) {
                    $employmentType = $this->get_employment_type($job);

                    if ($employmentType == null) {
                        continue;
                    }
                    
                    array_push($employmentTypes, $employmentType);
                }
            }

            $uniqueEmploymentType = array_unique($employmentTypes, SORT_REGULAR);
            return $uniqueEmploymentType;
        }

        // Employment type is a custom field and must be retrieved from within an array of metadata
        public function get_employment_type ($job) {
            if(!isset($job['metadata'])) {
                return null;
            }
            
            foreach($job['metadata'] as $metadata) {
                if ($metadata['id'] == $this->employment_type_field_id) {
                    return $metadata['value'];
                }
            }

            return null;
        }

        private function greenhouse_get_jobs () {
            // if using locally without proper ssl use: wp_remote_get(path, array('sslverify' => FALSE))
            $jobsResponse = wp_remote_get(get_stylesheet_directory_uri() . '/greenhouse-jobs.json');
            $jobsResponseBody = wp_remote_retrieve_body($jobsResponse);
            $jobs = json_decode($jobsResponseBody, true)['jobs'];

            return $jobs;
        }

        private function greenhouse_get_offices () {
            // if using locally without proper ssl use: wp_remote_get(path, array('sslverify' => FALSE))
            $officeResponse = wp_remote_get(get_stylesheet_directory_uri() . '/greenhouse-offices.json');
            $officeResponseBody = wp_remote_retrieve_body($officeResponse);
            $offices = json_decode($officeResponseBody, true)['offices'];

            return $offices;
        }

        private function greenhouse_get_departments () {
            // if using locally without proper ssl use: wp_remote_get(path, array('sslverify' => FALSE))
            $departmentsResponse = wp_remote_get(get_stylesheet_directory_uri() . '/greenhouse-departments.json');
            $departmentsResponseBody = wp_remote_retrieve_body($departmentsResponse);
            $departments = json_decode($departmentsResponseBody, true)['departments'];

            return $departments;
        }

    }
    $greenhouseApi = new GreenhouseApi();
}