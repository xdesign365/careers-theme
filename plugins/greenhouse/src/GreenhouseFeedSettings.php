<?php

/**
 * Create Feed settings page.
 */
if ( ! class_exists('GreenhouseFeedSettings') ) {
    class GreenhouseFeedSettings {

        /**
         * GreenhouseFeedSettings constructor.
         */
        public function __construct() {
            add_action('admin_menu', array($this,'greenhouse_feed_settings_menu'));
            add_action('admin_init', array($this, 'greenhouse_feed_settings_register_settings'));
        }

        /**
         * Greenhouse Feed settings menu.
         */
        public function greenhouse_feed_settings_menu() {
            add_menu_page(
                __('Greenhouse Feed', 'greenhouse-feed'),
                __('Greenhouse Feed', 'greenhouse-feed'),
                'manage_options',
                'greenhouse-feed',
                array($this, 'greenhouse_feed_settings_menu_page'),
                WP_PLUGIN_URL . '/greenhouse/assets/icons/greenhouse.svg',
                70
            );
        }

        /**
         * Greenhouse Feed register settings.
         */
        public function greenhouse_feed_settings_register_settings() {
            // Add option to Database
            add_option('api_key', '', '', 'yes');
            // Register settings that this form is allowed to update.
            register_setting('greenhouse-feed', 'api_key');
        }

        public function greenhouse_feed_settings_menu_page() {
            
            // Check if user have permission.
            if (!current_user_can('manage_options')) {
                wp_die(__("You don't have access to this page"));
            }
            ?>

            <div class="wrap">
            <h2>Greenhouse Feed</h2>
                <?php
                // print_r((new GreenhouseFeed())->get_body());
                ?>
            <form method="post" action="options.php">
                <table class="form-table">
                    <tr>
                        <th scope="row">API Key</th>
                        <td>
                            <label>
                                <input type="password" name="api_key" value="<?php echo esc_attr( get_option('api_key') ); ?>" />
                            </label>
                        </td>
                    </tr>
                </table>
                <?php
                settings_fields("greenhouse-feed");
                do_settings_sections("greenhouse-feed");
                submit_button('Save');
                ?>
            </form>
            </div>

            <?php
        }
    }

    $greenhouseFeedSettings = new GreenhouseFeedSettings();
}