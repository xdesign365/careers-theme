<?php
/**
 * Plugin Name: Greenhouse
 * Plugin URI: https://www.xdesign.com
 * Description: Custom plugin based off original Workable plugin that uses greenhouse api data that is stored in this theme from a cron job. It is used for the custom stats on the hero-a gutenberg block and also comes with a custom Greenhouse Jobs block that require this plugin to be active to work.
 * Version: 1.0
 * Author: xDesign
 * Author URI: https://www.xdesign.com
 */

require_once 'src/GreenhouseApi.php';
require_once 'src/GreenhouseAjax.php';		


if (!class_exists('GreenhouseFeed')) {
    class GreenhouseFeed
    {
        public function __construct()
        {
            add_shortcode('GreenhouseFeed', array($this, 'shortcode'));
            $this->enqueue_scripts();
        }

        public function shortcode() {

            // Job Apply
            if ( isset( $_GET['gh_jid'] ) )
            {
                $output = '<div id="grnhse_app"></div><script src="https://boards.eu.greenhouse.io/embed/job_board/js?for=xdesign"></script>';
            } 
            
            // Job Listings
            else {
                // Greenhouse API main wrapper
                $output = '<div class="greenhouse-api container">';
                            
                // Greenhouse API main content wrapper
                $output .= '<div class="greenhouse-api__main-content">';

                $output .= $this->get_filters();

                // Loader
                $output .= '<div class="jobs-loader"></div>';
                // Message if none of the job is found
                $output .= '<p class="greenhouse-api__not-found-msg">Sorry, no jobs matching your search criteria could be found.</p>';
                $output .= '<div class="greenhouse-api__jobs-content">';
                $output .= $this->get_body();

                // Closing tag of jobs content wrapper
                $output .= '</div>';

                // Closing tag of "greenhouse-api__main-content"
                $output .= '</div>';
                // Closing tag of "greenhouse-api" main wrapper
                $output .= '</div>';
            }

            return $output;
        }

        public function get_filters($search_string = NULL,$location = NULL,
                                    $department = NULL, $work_type = NULL,
                                    $remote_only = FALSE) {

            $greenhouse_api_filters = (new GreenhouseApi)->greenhouse_feed_filters();

            $filters = '';

            // Filters Wrapper
            $filters .= '<div class="greenhouse-api__filters">';
            
            // Search container & input
            $filters .= '<div class="greenhouse-api__search">';
            $filters .= '<label><span class="sr-only">Search jobs at xDesign</span><input placeholder="Search jobs..." type="search" id="-mains-earch" name="main-search"></label>';
            $filters .= '</div>';

            // Locations filter
            $filters .= '<div class="greenhouse-api__select greenhouse-api__location">';
            $filters .= '<div>';

            // Location list
            $filters .= '<label for="location-select">Location';
            $filters .= '<select name="location" id="location-select">';
            // Loop to get location names
            foreach ($greenhouse_api_filters['locations'] as $location) {

                // Location list item
                $filters .= '<option>';
                // Location name
                $filters .= $location;
                $filters .= '</option>';
            }
            $filters .= '</select>';
            $filters .= '</label>';

            $filters .= '</div>';
            $filters .= '</div>';

            // Department filter
            $filters .= '<div class="greenhouse-api__select greenhouse-api__department">';
            $filters .= '<div>';


            // Department list
            $filters .= '<label for="department-select">Department';
            $filters .= '<select name="department" id="department-select" class="dropdown--tiered">';

            // Loop to get department names
            foreach ($greenhouse_api_filters['departments'] as $department) {

                // Department list item
                $filters .= '<option class="option-group" value="' . $department['id'] . '">';
                // Department name
                $filters .= $department['name'];
                $filters .= '</option>';

                foreach ($department['sub_departments'] as $sub_department) {
                    // Department list item
                    $filters .= '<option class="sub-option" value="' . $sub_department['id'] . '">';
                    // Department name
                    $filters .= $sub_department['name'];
                    $filters .= '</option>';                
                }
            }
            $filters .= '</select>';
            $filters .= '</label>';

            $filters .= '</div>';
            $filters .= '</div>';


            // Work Type filter
            $filters .= '<div class="test greenhouse-api__select greenhouse-api__work-type">';
            $filters .= '<div>';

            $filters .= '<label for="work-type-select">Employment Type';
            $filters .= '<select name="work-type" id="work-type-select">';
            // Loop to get work types names
            foreach ($greenhouse_api_filters['employment_type'] as $work_type) {

                // Work type list item
                $filters .= '<option>';
                // Location name
                $filters .= $work_type;
                $filters .= '</option>';
            }
            $filters .= '</select>';
            $filters .= '</label>';

            $filters .= '</div>';
            $filters .= '</div>';

            // Reset button
            $filters .= '<div class="greenhouse-api__reset-filters-wrapper">';
                $filters .= '<a class="greenhouse-api__reset-filters-button"></a>';
            $filters .= '</div>';

            // Closing tag of "greenhouse-api__filters"
            $filters .= '</div>';

            return $filters;

        }

        public function get_body($search_string = NULL, $location = NULL,
                                 $department = NULL, $work_type = NULL) {
            
            $greenhouse_api = new GreenhouseApi;
            $greenhouse_api_jobs = $greenhouse_api->greenhouse_feed_jobs();

            $content = '';

            // Jobs content wrapper
            $i = 0;
            foreach($greenhouse_api_jobs as $job) {
                $lower_location_name = isset($job['location']) ? strtolower($job['location']['name']) : '';

                 if ($search_string !== NULL || strlen($search_string) > 0) {
                    if(stristr($job['title'], $search_string) === FALSE && stristr($job['description'], $search_string) === FALSE) {
                        continue;
                    }
                 }

                if ($location !== NULL) {
                    $lower_location = strtolower($location);
                    if (!str_contains($lower_location_name, $lower_location)) {
                        continue;
                    }
                }

                if(get_field('department_select')) {
                    $acf_selected_department = get_field('department_select');
                }

                if ($department !== NULL) {
                    if (isset($job['departments'])) {
                        $jobHasDepartment = $this->contains_department($job['departments'], $department);
                        if (!$jobHasDepartment) {
                            continue;
                        }
                    }
                } elseif (isset($acf_selected_department) && $acf_selected_department != 'Unset') {
                    if (isset($job['department'])) {
                        if (strpos(strtolower($job['department']), strtolower($acf_selected_department)) === FALSE) {
                            continue;
                        }
                    }
                }

                if ($work_type !== NULL) {
                    $jobEmploymentType =  $greenhouse_api->get_employment_type($job);
                    if (isset($jobEmploymentType) && $jobEmploymentType != $work_type) {
                        continue;
                    }
                }

                // Creating job to be clickable
                $content .= '<a href="';
                $content .= $job['absolute_url'];
                $content .= '" rel="noopener noreferrer" target="_blank">';

                // Single job wrapper
                $content .= '<div class="greenhouse-api__job">';

                // Field for job information and date
                $content .= '<div class="greenhouse-api__job-information">';

                $content .= '<h2 class="greenhouse-api__job-title">';
                $content .= $job['title'];
                $content .= '</h2>';

                $content .= '<p class="greenhouse-api__job-subtitle">';
                $content .=  $job['location']['name'];
                $content .= "</p>";

                $content .= '</div>';

                // // Field for job location
                // $content .= '<div class="greenhouse-api__job-location">';

                // $content .= isset($job['location']['region']) && strlen($job['location']['region']) > 0 ? '<h3 class="greenhouse-api__city">' : '';
                // $content .= $job['location']['city'];
                // $content .= isset($job['location']['region']) && strlen($job['location']['region']) > 0 ? ', ' . $job['location']['region'] : '';
                // $content .= isset($job['location']['region']) && strlen($job['location']['region']) > 0 ? '</h3>' : '';

                // $content .= '<p class="greenhouse-api__country">';
                // $content .= $job['location']['name'];
                // $content .= '</p>';

                // $content .= '</div>';

                // Field for job position
                $content .= '<div class="greenhouse-api__job-position">';
                $content .= '<p class="greenhouse-api__position">';
                $content .= $job['departments'][0]['name'];
                $content .= '</p>';
                $content .= '</div>';

                // Field for work type
                $content .= '<div class="greenhouse-api__job-arrow">';
                $content .= '<svg class="" width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">';
                $content .= '<path d="M15.693 10.3328L0.79999 10.3328L0.799991 7.66766L15.693 7.66766L11.1329 1.85757L13.0608 0.151857L20 9.00558L13.0608 17.8486L11.1329 16.1429L15.693 10.3328Z" fill="currentColor"></path>';
                $content .= '</svg>';
                $content .= '</div>';

                $content .= '</div>';

                // Closing tag of anchor for job
                $content .= '</a>';

                $i++;

                // if($i == 10) {
                //     break;
                // }
            }

            return $content;

        }

        public function enqueue_scripts() {
            wp_enqueue_style('nice-select-2-style', WP_PLUGIN_URL . '/greenhouse/assets/nice-select2/css/nice-select2.css', '', '', false);
            wp_enqueue_style('nice-select-2-style-main', WP_PLUGIN_URL . '/greenhouse/assets/nice-select2/css/style.css', '', '', false);
            wp_enqueue_script('nice-select-2-script', WP_PLUGIN_URL . '/greenhouse/assets/nice-select2/js/nice-select2.js', '', '', true);
            wp_enqueue_script('greenhouse-main-script', WP_PLUGIN_URL . '/greenhouse/assets/js/main.js', '', '', true);
        }

        private function contains_department ( $jobDepartments, $targetDepartmentName) {
            foreach($jobDepartments as $department) {
                if ($department['id'] == $targetDepartmentName) {
                    return true;
                }

                if ($department['parent_id'] == $targetDepartmentName) {
                    return true;
                }

            }

            return false;
        }

    }

    $greenhouseApi = new GreenhouseFeed();





    // $greenhouse_api_filters = (new GreenhouseApi)->greenhouse_feed_filters();


    // $departmentsArray = $greenhouse_api_filters['departments'];

    // $acf_select = ['Unset' => 'Unset'];

    // foreach($departmentsArray as $single_department) {
    //     $acf_select[$single_department['name']] = $single_department['name'];
    // }			

    // if( function_exists('acf_add_local_field_group') ):

    //     acf_add_local_field_group(array(
    //         'key' => 'group_61b72e0bcab9c',
    //         'title' => 'Block: Greenhouse API',
    //         'fields' => array(
    //             array(
    //                 'key' => 'field_61b72f753ebc5',
    //                 'label' => 'Department select',
    //                 'name' => 'department_select',
    //                 'type' => 'select',
    //                 'instructions' => 'Set a default department/category of jobs to initially show.',
    //                 'required' => 0,
    //                 'conditional_logic' => 0,
    //                 'wrapper' => array(
    //                     'width' => '',
    //                     'class' => '',
    //                     'id' => '',
    //                 ),
    //                 'choices' => $acf_select,
    //                 'default_value' => false,
    //                 'allow_null' => 0,
    //                 'multiple' => 0,
    //                 'ui' => 0,
    //                 'return_format' => 'label',
    //                 'ajax' => 0,
    //                 'placeholder' => '',
    //             ),
    //         ),
    //         'location' => array(
    //             array(
    //                 array(
    //                     'param' => 'block',
    //                     'operator' => '==',
    //                     'value' => 'acf/greenhouse-api',
    //                 ),
    //             ),
    //         ),
    //         'menu_order' => 0,
    //         'position' => 'normal',
    //         'style' => 'default',
    //         'label_placement' => 'top',
    //         'instruction_placement' => 'label',
    //         'hide_on_screen' => '',
    //         'active' => true,
    //         'description' => '',
    //         'show_in_rest' => 0,
    //     ));
        
    //     endif;		
}
