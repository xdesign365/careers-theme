# README #
The initial theme and technologies in this repo were supplied by forge.uk and were massaged to enable them to function prior to placing on AWS Lightsail

## PASSWORDS / CREDENTIALS
All bitnami credentials are stored in following home folder files:
bitnami_credentials
bitnami_application_password

The Workable plugin and associated cronjobs run with an API key available from Workable administration.

## SFTP

To access the box download the `default key` and store appropriately locally and utilise via:
`ssh -i LightsailDefaultKey-eu-west-2.pem bitnami@18.130.113.186`

## PHPMYADMIN
`ssh -N -L 8888:127.0.0.1:80 -i LightsailDefaultKey-eu-west-2.pem bitnami@18.130.113.186`

Then in your browser access the following URL:
`http://127.0.0.1:8888/phpmyadmin/index.php`

username:password

root:{bitnami_application_password}


## CRONJOBS
sudo crontab -e

* * * * *  /bin/sh -c "curl --silent 'https://apply.workable.com/api/v1/widget/accounts/xdesign/departments' > /opt/bitnami/wordpress/wp-content/themes/xdesign/workable-departments.json"

* * * * *  /bin/sh -c "curl --silent -H 'Authorization: Bearer {workable_api_key}' 'https://xdesign.workable.com/spi/v3/jobs?state=published&limit=100&include_fields=employment_type,description' > /opt/bitnami/wordpress/wp-content/themes/xdesign/workable-jobs.json"


## LIGHTSAIL SETUP / CONFIGURATION

/opt/bitnami/wordpress/

/opt/bitnami/apache2/bin/apachectl configtest

sudo /opt/bitnami/ctlscript.sh restart apache

/opt/bitnami/apache/conf/httpd.conf

/opt/bitnami/apache/conf/bitnami.conf

/opt/bitnami/apache/conf/vhosts/wordpress-vhost.conf


### FILE PERMISSIONS
Lightsail's standard installation needs to be tweaked before it can be used. 

Files need to be owned by `bitnami:daemon` to ensure they have the correct owner for serving (in particular if you upload via SFTP):

`sudo find /opt/bitnami/wordpress -type d -exec chmod 775 {} \;`

`sudo find /opt/bitnami/wordpress -type f -exec chmod 664 {} \;`

